Admin = require("../model/admin");
otp = require("../model/otp");
user = require('../model/user');
booking = require('../model/booking');
hotel = require('../model/hotel');
feedback = require('../model/feedback');
activity = require('../model/activity');
paymentHistory = require('../model/paymentHistory');
var commonMethodes = require('../route/commonMethodes');
var notification = require('../notification/notify');

var mongoose = require("mongoose");
var ObjectId = mongoose.Types.ObjectId;
var moment = require('moment');


exports.loginAdmin = function (req, res) {
    console.log("loginAdmin");
    let adminEmail = req.body.email;
    let adminPassword = req.body.password;
    console.log("email and password", adminEmail, adminPassword);
    user.findOne(
        {
            email: adminEmail,
            password: adminPassword
        },
        function (err, data) {
            if (err) {
                res.json({
                    error: true,
                    message: err
                });
            } else if (data) {
                res.json({ error: false, "message": "Login successfully", "data": data });
            } else {
                res.json({ error: true, "message": "Something went wrong" });
            }
        }
    );
};

exports.getSingleUser = function (req, res) {
    console.log("getSingleUser");
    var userId = req.body.user_id;

    user.aggregate([{ $match: { _id: ObjectId(userId) } }, { $lookup: { from: 'bookings', localField: '_id', foreignField: 'buyer_id', as: 'bookingData' } }], function (err, data) {
        if (err) {
            console.log("error", err);
            res.json({ error: true, message: "Something went wrong" });
        } else {
            if (data) {
                res.json({ error: false, message: "User Detail", "data": data });
            } else {
                res.json({ error: true, message: "Data not found" });
            }
        }
    })
};

exports.listUser = function (req, res) {
    console.log("listUser");
    let number = req.body.number;
    let query = number * 2;
    user.aggregate([{
        $match: {
            is_blocked: false
        }
    }, {
        "$skip": query
    }, {
        "$limit": 2
    }], function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else if (data.length == 0) {
            res.json({
                error: false,
                data: data
            });
        } else {
            res.json({
                error: false,
                data: data
            });
        }
    })
}

exports.listRegexUser = function (req, res) {
    console.log("listRegexUser");
    let word = req.body.word;
    let number = req.body.number;
    let query = number * 2;
    user.find({
        name: {
            $regex: ".*" + word + ".*",
            $options: 'si'
        },
        is_blocked: false
    }, function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else {
            res.json({
                error: false,
                data: data
            })
        }
    }).skip(query).limit(2)
}

exports.CountUsers = function (req, res) {
    console.log("CountUsers");
    user.aggregate([{
        $count: "countUser"
    }], function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else if (!data) {
            res.json({
                error: true,
                message: "No Data Found"
            });
        } else {
            res.json({
                error: false,
                data: data
            });
        }
    });
}

exports.CountHotels = function (req, res) {
    console.log("CountHotels");
    hotel.aggregate([{ $match: { is_deleted: false, is_approved: true } }, {
        $count: "countHotel"
    }], function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else if (!data) {
            res.json({
                error: true,
                message: "No Data Found"
            });
        } else {
            res.json({
                error: false,
                data: data
            });
        }
    });
}

exports.CountBlockedUsers = function (req, res) {
    console.log("CountBlockedUsers");
    user.aggregate([{ $match: { is_blocked: true } }, {
        $count: "countBlockedUser"
    }], function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else if (!data) {
            res.json({
                error: true,
                message: "No Data Found"
            });
        } else {
            res.json({
                error: false,
                data: data
            });
        }
    });
}

exports.CountUnapprovedHotels = function (req, res) {
    console.log("CountUnapprovedHotels");
    hotel.aggregate([{ $match: { is_approved: false, is_deleted: false } }, {
        $count: "countUnapprovedHotel"
    }], function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else if (!data) {
            res.json({
                error: true,
                message: "No Data Found"
            });
        } else {
            res.json({
                error: false,
                data: data
            });
        }
    });
}

exports.listBlockUser = function (req, res) {
    console.log("listBlockUser");
    let number = req.body.number;
    let query = number * 2;
    user.aggregate([{
        $match: {
            is_blocked: true
        }
    }, {
        "$skip": query
    }, {
        "$limit": 2
    }], function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else if (data.length == 0) {
            res.json({
                error: false,
                data: data
            });
        } else {
            res.json({
                error: false,
                data: data
            });
        }
    })
}

exports.listBlockRegexUser = function (req, res) {
    console.log("listBlockRegexUser");
    let word = req.body.word;
    let number = req.body.number;
    let query = number * 15;

    user.find({
        name: {
            $regex: ".*" + word + ".*",
            $options: 'si'
        },
        is_blocked: true
    }, function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else {
            res.json({
                error: false,
                data: data
            })
        }
    }).skip(query).limit(15)
}

exports.listFarm = function (req, res) {
    console.log("listFarm");
    let number = req.body.number;
    let query = number * 15;
    hotel.aggregate([{
        $match: {
            is_deleted: false,
            is_approved: true,
            type: "Farm"
        }
    }, {
        "$skip": query
    }, {
        "$limit": 15
    }], function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else if (data.length == 0) {
            res.json({
                error: false,
                data: data
            });
        } else {
            res.json({
                error: false,
                data: data
            });
        }
    })
}

exports.listRegexFarm = function (req, res) {
    console.log("listRegexFarm");
    let word = req.body.word;
    let number = req.body.number;
    let query = number * 15;

    hotel.find({
        name: {
            $regex: ".*" + word + ".*",
            $options: 'si'
        },
        is_approved: true,
        is_deleted: false,
        type: "Farm"
    }, function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else {
            res.json({
                error: false,
                data: data
            })
        }
    }).skip(query).limit(15)
}

exports.listCamp = function (req, res) {
    console.log("listCamp");
    let number = req.body.number;
    let query = number * 15;
    hotel.aggregate([{
        $match: {
            is_deleted: false,
            is_approved: true,
            type: "Camp"
        }
    }, {
        "$skip": query
    }, {
        "$limit": 15
    }], function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else if (data.length == 0) {
            res.json({
                error: false,
                data: data
            });
        } else {
            res.json({
                error: false,
                data: data
            });
        }
    })
}

exports.listRegexCamp = function (req, res) {
    console.log("listRegexCamp");
    let word = req.body.word;
    let number = req.body.number;
    let query = number * 15;

    hotel.find({
        name: {
            $regex: ".*" + word + ".*",
            $options: 'si'
        },
        is_approved: true,
        is_deleted: false,
        type: "Camp"
    }, function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else {
            res.json({
                error: false,
                data: data
            })
        }
    }).skip(query).limit(15)
}

exports.listChalet = function (req, res) {
    console.log("listChelet");
    let number = req.body.number;
    let query = number * 15;
    hotel.aggregate([{
        $match: {
            is_deleted: false,
            is_approved: true,
            type: "Chalet"
        }
    }, {
        "$skip": query
    }, {
        "$limit": 15
    }], function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else if (data.length == 0) {
            res.json({
                error: false,
                data: data
            });
        } else {
            res.json({
                error: false,
                data: data
            });
        }
    })
}

exports.listRegexChalet = function (req, res) {
    console.log("listRegexChelet");
    let word = req.body.word;
    let number = req.body.number;
    let query = number * 15;

    hotel.find({
        name: {
            $regex: ".*" + word + ".*",
            $options: 'si'
        },
        is_approved: true,
        is_deleted: false,
        type: "Chalet"
    }, function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else {
            res.json({
                error: false,
                data: data
            })
        }
    }).skip(query).limit(15)
}

exports.listUnapprovedHotel = function (req, res) {
    console.log("listUnapprovedHotel");
    let number = req.body.number;
    let query = number * 15;
    hotel.aggregate([{
        $match: {
            is_deleted: false,
            is_approved: false
        }
    },
    { $lookup: { from: 'users', localField: 'owner_id', foreignField: '_id', as: 'userData' } }
        , {
        "$skip": query
    }, {
        "$limit": 15
    }], function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else if (data.length == 0) {
            res.json({
                error: false,
                data: data
            });
        } else {
            res.json({
                error: false,
                data: data
            });
        }
    })
}

exports.listBooking = function (req, res) {
    console.log("listBooking");
    let number = req.body.number;
    let query = number * 5;
    booking.aggregate([
        { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } }
        , {
            "$skip": query
        }, {
            "$limit": 5
        }], function (error, data) {
            if (error) {
                res.json({
                    error: true,
                    message: error
                });
            } else if (data.length == 0) {
                res.json({
                    error: false,
                    data: data
                });
            } else {
                res.json({
                    error: false,
                    data: data
                });
            }
        })
}

exports.listFeedback = function (req, res) {
    console.log("listfeedback");
    let number = req.body.number;
    let query = number * 5;
    feedback.aggregate([
        { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } },
        { $lookup: { from: 'users', localField: 'user_id', foreignField: '_id', as: 'userData' } }
        , {
            "$skip": query
        }, {
            "$limit": 5
        }], function (error, data) {
            if (error) {
                res.json({
                    error: true,
                    message: error
                });
            } else if (data.length == 0) {
                res.json({
                    error: false,
                    data: data
                });
            } else {
                res.json({
                    error: false,
                    data: data
                });
            }
        })
}

exports.approvedHotel = function (req, res) {
    var hotelId = req.body.hotel_id;
    var sataus = req.body.is_approved;
    let obj = {};

    console.log("HotelId>>>>>>>>>>>>>>>>>>", hotelId, "status>>>>>>>>>>>", sataus);
    hotel.findOneAndUpdate({ _id: ObjectId(hotelId) }, { $set: { is_approved: sataus } }, function (err, data) {
        if (err) {
            console.log("Error>>>>>..", err);
        } else {
            if (data) {
                res.json({ error: false, "message": "Property updated successfully" });

                console.log("hotel data>>>>>>>>>>>>>>>>>>", data);
                user.findOne({ _id: ObjectId(data.owner_id) }, function (err, sale) {
                    if (err) {

                    } else {
                        // console.log("owner5 data at createbooking is >>>>>>>>", sale,"bookingId>>>>>>>>>>>>>",data._id);
                        console.log("owner Data at approved hotel api.js>>>>>>>>>>>>>>>>>>>>>>>>>>", sale);
                        if (sataus) {
                            obj = { "message": "Your property is approved by admin", "data": data._id, "type": "Approved" };
                        } else {
                            obj = { "message": "Your property is unapproved by admin", "data": data._id, "type": "Approved" };
                        }

                        if (sale.notification_enable) {
                            if (sale.device_type == "Android") {
                                notification.sendNotification([sale.notification_token], obj, data.owner_id, function (data1) {

                                });
                            } else {
                                console.log("IOS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                notification.sendNotificationForIOS([sale.notification_token], obj, obj.message, "message", "Approved", data.owner_id, function (data1) {

                                });
                            }
                        }

                        // commonMethodes.data.updateActivity(data.buyer_id, "You paid for a booking", "Book", bookingId, data.hotel_id, function (result) {
                        //     if (result == true) {
                        //         console.log("Activity Saved for Buyer");
                        //     }
                        // });
                        commonMethodes.data.updateActivity(sale._id, obj.message, "Approved", hotelId, hotelId, function (result) {
                            if (result == true) {
                                console.log("Activity Saved for Seller");
                            }
                        });
                    }
                })
            } else {
                res.json({ error: true, "message": "Something went wrong" });
            }
        }
    })
}

exports.deleteUnapprovedHotel = function (req, res) {
    var hotelId = req.body.hotel_id;

    hotel.remove({ _id: ObjectId(hotelId) }, function (err, data) {
        if (err) {
            console.log("Error>>>>>..", err);
        } else {
            if (data) {
                res.json({ error: false, "message": "Property deleted successfully" });
            } else {
                res.json({ error: true, "message": "Something went wrong" });
            }
        }
    })
}

exports.listTransaction = function (req, res) {
    console.log("listTransaction");
    let number = req.body.number;
    let query = number * 2;
    paymentHistory.aggregate([{
        "$sort": { created_at: -1 }
    },
    { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } },
    { $lookup: { from: 'users', localField: 'buyer_id', foreignField: '_id', as: 'buyerData' } },
    { $lookup: { from: 'bookings', localField: 'booking_id', foreignField: '_id', as: 'bookingData' } }
        , {
        "$skip": query
    }, {
        "$limit": 2
    }], function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else if (data.length == 0) {
            res.json({
                error: false,
                data: data
            });
        } else {
            res.json({
                error: false,
                data: data
            });
        }
    })
}

exports.listBookingByUser = function (req, res) {
    var userId = req.body.user_id;
    let number = req.body.number;
    let query = number * 2;

    booking.aggregate([{ $match: { buyer_id: ObjectId(userId) } }, { "$sort": { created_at: -1 } }, { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } }, {
        "$skip": query
    }, {
        "$limit": 2
    }], function (err, data) {
        if (err) {
            console.log("Error>>>>>>>>>>", err);
        } else {
            if (data) {
                res.json({ error: false, "message": "Booking data", "data": data });
            } else {
                res.json({ error: true, "message": "Booking data not found" });
            }
        }
    })
}

exports.listPropertyByUser = function (req, res) {
    var userId = req.body.user_id;
    let number = req.body.number;
    let query = number * 2;

    hotel.aggregate([{ $match: { owner_id: ObjectId(userId) } }, { "$sort": { created_at: -1 } }, { $lookup: { from: 'users', localField: 'owner_id', foreignField: '_id', as: 'ownerData' } }, {
        "$skip": query
    }, {
        "$limit": 2
    }], function (err, data) {
        if (err) {
            console.log("Error>>>>>>>>>>", err);
        } else {
            if (data) {
                res.json({ error: false, "message": "Booking data", "data": data });
            } else {
                res.json({ error: true, "message": "Booking data not found" });
            }
        }
    })
}

exports.updateBlock = function (req, res) {
    var userId = req.body.user_id;
    var block = req.body.block;

    user.updateOne({ _id: ObjectId(userId) }, { $set: { is_blocked: block } }, function (err, data) {
        if (err) {
            console.log("Error>>>>>>>>>>", err);
        } else {
            if (data) {
                res.json({ error: false, "message": "User blocked updated" });
            } else {
                res.json({ error: true, "message": "something went wrong" });
            }
        }
    })
}

exports.getSingleHotel = function (req, res) {
    console.log("getSingleHotel");
    var hotelId = req.body.hotel_id;

    hotel.aggregate([{ $match: { _id: ObjectId(hotelId) } }, { $lookup: { from: 'users', localField: 'owner_id', foreignField: '_id', as: 'ownerData' } }], function (err, data) {
        if (err) {
            console.log("error", err);
            res.json({ error: true, message: "Something went wrong" });
        } else {
            if (data) {
                res.json({ error: false, message: "hotel Detail", "data": data });
            } else {
                res.json({ error: true, message: "Data not found" });
            }
        }
    })
}

exports.getBookingByHotel = function (req, res) {
    var hotelId = req.body.hotel_id;
    let number = req.body.number;
    let query = number * 2;

    booking.aggregate([{ $match: { hotel_id: ObjectId(hotelId) } }, { "$sort": { created_at: -1 } }, { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } }, {
        "$skip": query
    }, {
        "$limit": 2
    }], function (err, data) {
        if (err) {
            console.log("Error>>>>>>>>>>", err);
        } else {
            if (data) {
                res.json({ error: false, "message": "Booking data", "data": data });
            } else {
                res.json({ error: true, "message": "Booking data not found" });
            }
        }
    })
}

exports.getTransactionByHotel = function (req, res) {
    console.log("getTransactionByHotel");
    let hotelId = req.body.hotel_id;
    let number = req.body.number;
    let query = number * 2;
    paymentHistory.aggregate([{ $match: { hotel_id: ObjectId(hotelId) } }, {
        "$sort": { created_at: -1 }
    },
    { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } },
    { $lookup: { from: 'users', localField: 'buyer_id', foreignField: '_id', as: 'buyerData' } },
    { $lookup: { from: 'bookings', localField: 'booking_id', foreignField: '_id', as: 'bookingData' } }
        , {
        "$skip": query
    }, {
        "$limit": 2
    }], function (error, data) {
        if (error) {
            res.json({
                error: true,
                message: error
            });
        } else if (data.length == 0) {
            res.json({
                error: false,
                data: data
            });
        } else {
            res.json({
                error: false,
                data: data
            });
        }
    })
}

exports.setTransferStatus = function (req, res) {
    var transactionId = req.body.transaction_id;

    paymentHistory.updateOne({ _id: ObjectId(transactionId) }, { $set: { status: true } }, function (err, data) {
        if (err) {
            console.log("error>>>>>>>>>>>", err);
        } else {
            if (data) {
                res.json({ error: false, "message": "Money transfer successfully" });
            } else {
                res.json({ error: true, "message": "something went wrong" });
            }
        }
    })

}

exports.changePassword = function (req, res) {
    var userId = req.body.user_id;
    var newPass = req.body.new_password;

    user.findOneAndUpdate({ _id: ObjectId(userId) }, { $set: { password: newPass } }, function (err, data) {
        if (err) {
            res.json({ error: true, "message": "Something went wrong" });
            console.log("Error>>>>>>>>>", err);
        } else {
            if (data) {
                res.json({ error: false, "message": "Password updated successfully" });
            } else {
                res.json({ error: true, "message": "Something went wrong" });
            }
        }
    })
}

exports.getBookingById = function (req, res) {
    console.log("getSingleBooking");
    var bookingId = req.body.booking_id;

    booking.aggregate([{ $match: { _id: ObjectId(bookingId) } }, { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } }, { $lookup: { from: 'users', localField: 'buyer_id', foreignField: '_id', as: 'buyerData' } },
    { $lookup: { from: 'users', localField: 'owner_id', foreignField: '_id', as: 'ownerData' } }], function (err, data) {
        if (err) {
            console.log("error", err);
            res.json({ error: true, message: "Something went wrong" });
        } else {
            if (data) {
                res.json({ error: false, message: "Booking Detail", "data": data });
            } else {
                res.json({ error: true, message: "Data not found" });
            }
        }
    })
}

exports.changePasswordByEmail = function (req, res) {
    var email = req.body.email;
    var newPass = req.body.new_password;

    user.findOneAndUpdate({ email: email }, { $set: { password: newPass } }, function (err, data) {
        if (err) {
            res.json({ error: true, "message": "Something went wrong" });
            console.log("Error>>>>>>>>>", err);
        } else {
            if (data) {
                res.json({ error: false, "message": "Password updated successfully" });
            } else {
                res.json({ error: true, "message": "Something went wrong" });
            }
        }
    })
}

exports.sumTransaction = function (req, res) {

    paymentHistory.aggregate([
        {
            $group:
            {
                _id: '',
                total: { $sum: '$amount' }
            }
        },
        // {
        //     $project: {
        //         _id: 0,
        //         amount: '$amount'
        //     }
        // }
    ], function (err, data) {
        if (err) {
            console.log("Error>>>>>>>", err);
        } else {
            if (data) {
                console.log("Data>>>>>>>>>>", data);
                res.json({ error: false, "message": "Sum of Transaction", "data": data });
            } else {
                res.json({ error: true, "message": "No data found" });
                console.log("No dat found");
            }
        }
    })
}

exports.getUserInSevenDay = function (req, res) {
    var array;
    var userArray = [];
    var day = moment().format("YYYY-MM-DD");
    console.log("now >>>>>>>>>", day);
    let count = 7;

    for (let i = 0; i < 7; i++) {
        array = moment().subtract(i, 'd').format("YYYY-MM-DD");
        console.log("Array of date >>>>>>>>", array);

        let ans = [{
            "_id": array,
            "count": 0
        }];

        user.aggregate([
            { $match: { date_for_fetch: array } }
            ,
            {
                $group:
                {
                    _id: array,
                    count: { $sum: 1 }
                }
            }
        ], function (error, data) {
            count--;
            if (error) {
                console.log("Error>>>>>>>>>>", error);
            } else if (data.length > 0) {

                console.log("data>>>>>>>>>", data);
                userArray[i] = data;
                //i++;
                if (count == 0) {
                    res.json({
                        error: false,
                        "data": userArray
                    });
                }
            } else {

                console.log("data11111111111>>>>>>>>>", data);
                userArray[i] = ans;
                // i++;
                if (count == 0) {
                    res.json({
                        error: false,
                        "data": userArray
                    });
                }
            }
        })
    }
}

exports.getTransactionInDays = function (req, res) {
    var array;
    var userArray = [];
    var day = moment().format("YYYY-MM-DD");
    console.log("now >>>>>>>>>", day);
    let count = 15;

    for (let i = 0; i < 15; i++) {
        array = moment(Date.now()).subtract(i, 'd').format('YYYY-MM-DD');

        //let milli = Date.parse(array).toString();
        console.log("Array of date >>>>>>>>", array);

        let ans = [{
            "_id": array,
            "totalAmount": 0
        }];

        paymentHistory.aggregate([
            { $match: { date_for_fetch: array } }
            ,
            {
                $group:
                {
                    _id: array,
                    totalAmount: { $sum: '$amount' }
                }
            }
        ], function (error, data) {
            count--;
            if (error) {
                console.log("Error>>>>>>>>>>", error);
            } else if (data.length > 0) {

                console.log("data>>>>>>>>>", data);
                userArray[i] = data;
                //i++;
                if (count == 0) {
                    res.json({
                        error: false,
                        "data": userArray
                    });
                }
            } else {

                console.log("data11111111111>>>>>>>>>", data);
                userArray[i] = ans;
                // i++;
                if (count == 0) {
                    res.json({
                        error: false,
                        "data": userArray
                    });
                }
            }
        })
    }
}

