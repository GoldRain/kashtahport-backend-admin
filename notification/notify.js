var user = require('../model/user');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;


exports.sendNotification = function (ids, messageDetail, userId, next) {
    console.log("notificaton function called : ", ids, ' : messagedetails : ', messageDetail)
    var data = {
        app_id: "134846ad-d9da-4221-8ee1-cc594387cdb5",
        content_available: true,
        data: { "message": messageDetail, "type": "message" },
        include_player_ids: ids,
        ios_badgeType: "Increase",
        ios_badgeCount: "1"
    };

    var headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": "Basic MzE3YjMxYjMtNjE0Yi00ZmQ3LThmOTYtNTJhMjYwMTc5MGJm"
    };

    var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
    };


    var https = require('https');
    var req = https.request(options, function (res) {
        res.on('data', function (data) {
            console.log("Response:>>>>>>>>>>>>>>>>>>>", data);
            console.log(JSON.parse(data));
            next(true);
        });
    });

    req.on('error', function (e) {
        console.log("ERROR:");
        console.log(e);
        next(false);
    });

    req.write(JSON.stringify(data));
    req.end();
};

exports.sendNotificationForIOS = function (ids, messageDetail, messageToBeShown, type, titleForIOS, userId, next) {
    console.log("IOS notification called ids: ", ids, " - messageToBeShown: ", messageToBeShown, " - type: ", type, " - title: ", titleForIOS);
    var count;

    user.findOne({ _id: ObjectId(userId) }, function (err, data) {
        if (err) {
            console.log("Error>>>>>>>...", err);
        } else {
            if (data) {
                count = parseInt(data.count);
                user.updateOne({ _id: ObjectId(userId) }, { $set: { count: count + 1 } }, function (err, finaldata) {
                    if (err) {
                        console.log("Error>>>", err);
                    } else {
                        if (finaldata) {
                            console.log("Count plus by One>>>>>>");
                            var data = {
                                app_id: "134846ad-d9da-4221-8ee1-cc594387cdb5",
                                contents: { "en": messageToBeShown },
                                headings: { "en": titleForIOS },
                                ios_badgeType: "SetTo",
                                ios_badgeCount: count + 1,
                                data: { "message": messageDetail, "type": type, "sound": "ring.mp3" },
                                include_player_ids: ids
                            };

                            var headers = {
                                "Content-Type": "application/json; charset=utf-8",
                                "Authorization": "Basic MzE3YjMxYjMtNjE0Yi00ZmQ3LThmOTYtNTJhMjYwMTc5MGJm"
                            };

                            var options = {
                                host: "onesignal.com",
                                port: 443,
                                path: "/api/v1/notifications",
                                method: "POST",
                                headers: headers
                            };


                            var https = require('https');
                            var req = https.request(options, function (res) {
                                res.on('data', function (data) {
                                    console.log("Response:");
                                    console.log(JSON.parse(data));
                                    next(true);
                                });
                            });

                            req.on('error', function (e) {
                                console.log("ERROR:");
                                console.log(e);
                                next(false);
                            });

                            req.write(JSON.stringify(data));
                            req.end();
                        }
                    }
                })
            }
        }
    })
};
