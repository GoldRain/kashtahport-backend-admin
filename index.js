var express = require('express');
var app = express();
var route = require('./route/route');
var http = require('http').Server(app);
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var bodyParser = require('body-parser');
const path = require('path');
var schedule = require('node-schedule');
var scheduleBooking = require('./model/scheduleBooking');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(express.static('public'));

app.use(function(req, res, next) {
    //  res.setHeader('Access-Control-Allow-Origin', '192.168.21.118:4080');
     res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

app.use("/public", express.static(path.join(__dirname, '/public')));
// app.use('/public/voice.xml',express.static(path.join(__dirname, 'public/voice')));

app.use(express.static(path.join(__dirname, 'public/index')));
app.use(express.static(path.join(__dirname, 'public/term')));
app.use(express.static(path.join(__dirname, 'public/termArbic')));
app.use(express.static(path.join(__dirname, 'public/aboutus')));
app.use(express.static(path.join(__dirname, 'public/aboutusArbic')));
app.use(express.static(path.join(__dirname, 'public/policy')));
app.use(express.static(path.join(__dirname, 'public/policyArbic')));
app.use(express.static(path.join(__dirname, 'public/voice')));


app.use("/route", express.static(path.join(__dirname, '/route')));
app.use(express.static(path.join(__dirname, 'route/voice.xml')));

// function for routes
route(app);

//establish connection 
http.listen(8080, function () {
    console.log('listening on *:8080');

    // var j = schedule.scheduleJob('*/5 * * * *', function(){
    //     //console.log('The answer to life, the universe, and everything!');

    // });

    scheduleBooking.find({}, function(err, scheduleData){
        if(err){
            console.log("in index.js error occurred while scheduling job ", err);
        }
        else{
            if(scheduleData.length > 0){
                scheduleData.forEach(message => {
                    date = parseInt(message.scheduled_at);
                    checkDate = Date.now();
                    if(date <= checkDate)
                    {
                        console.log("let seee in true", date);
            
                    }
                    else{
                        console.log("let see in false", date )
                        var j = schedule.scheduleJob(date, function() {
                            console.log("if server restart");
                        });
                    }
                })
            }
        }
    });
    
});