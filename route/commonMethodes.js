var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var activity = require('../model/activity');
var otp = require('../model/otp');

//for send otp
//const curl = new (require('curl-request'))();
const accountSid = 'AC288df15a07e81a44c24a3a156f416da0';
const authToken = '01c9e6b8d8e1d9cec4f03ee8c4d98d8d';
const client = require('twilio')(accountSid, authToken);
const VoiceResponse = require('twilio').twiml.VoiceResponse;


var methods = {

    updateActivity: function (userId, message, hint, hintId, hotelId, next) {
        var act = new activity({
            user_id: ObjectId(userId),
            message: message,
            hint: hint,
            hint_id: ObjectId(hintId),
            hotel_id: ObjectId(hotelId),
            created_at: Date.now()
        })

        act.save(function (err, result) {
            if (err) {
                next(false);
            } else {
                if (result) {
                    next(true);
                }
            }
        })
    },

    send: function (phone, country_code, next) {
        console.log("check in send function", phone, country_code);
        var otp1 = Math.floor(100000 + Math.random() * 900000);
        // let otp1 = 123456;
        let registerOTP = new otp({
            phone: phone,
            otp: otp1,
            created_at: Date.now().toString(),
            expire_at: (Date.now() + 7200000).toString()
        })

        phoneNumber = "+" + country_code + phone;

        user.findOne({ phone: phone }, function (err, doc) {
            if (err) {
                res.json({ "error": true, "message": "Database Error!" });
            } else {
                if (doc) {
                    console.log("In Doc");
                    if (doc && doc.is_blocked) {
                        console.log("In Doc and doc.is_blocked");
                        res.json({ "error": true, "message": "Account BLOCKED!!! Please contact admin." });
                    } else {
                        //Delete past OTPs
                        console.log("In else of Doc and doc.is_blocked");
                        otp.deleteMany({ phone: phone }, function (err, deleteDone) {
                            if (err) {
                                res.json({ "error": true, "message": "Database Error!" });
                            } else {
                                if (deleteDone) {
                                    console.log("Deleting old otp is successful", deleteDone);
                                }

                                registerOTP.save(function (err, otpData) {
                                    if (err) {
                                        //res.json({ error: true, message: "Something went wrong, Please try again", data: err })
                                    } else {
                                        str = "Your OTP for KASHTAHPORT is: " + otp1;
                                        // request 
                                        console.log("new otp is saved", otpData);
                                        client.messages
                                            .create({
                                                body: str,
                                                from: '+12519298355',
                                                to: phoneNumber
                                            })
                                            .then(message => {
                                                console.log(">>>>>>>>> ", message)
                                            });
                                        next({ error: false, message: "OTP has been sent.", data: otpData });
                                    }
                                })
                            }
                        })
                    }
                } else {
                    console.log("If User Not Find By Phone");
                    otp.deleteMany({ phone: phone }, function (err, deleteDone) {
                        if (err) {
                            res.json({ "error": true, "message": "Database Error!" });
                        } else {
                            if (deleteDone) {
                                console.log("Deleting old otp is successful if user not found", deleteDone);
                            }

                            registerOTP.save(function (err, otpData) {
                                if (err) {
                                    //res.json({ error: true, message: "Something went wrong, Please try again", data: err })
                                } else {
                                    str = "Your OTP for KASHTAHPORT is: " + otp1;
                                    // request 
                                    client.messages
                                        .create({
                                            body: str,
                                            from: '+12519298355',
                                            to: phoneNumber
                                        })
                                        .then(message => {
                                            console.log(">>>>>>>>> ", message)
                                        });
                                    next({ error: false, message: "OTP has been sent.", data: otpData });
                                }
                            })
                        }
                    })
                }
            }
        })
    },

    call: function (phone, country_code) {
        console.log("call methode is called");
        phoneNumber = "+" + country_code + phone;
        client.calls
            .create({
                url: 'http://18.191.10.69:8080/voice.xml',
                to: phoneNumber,
                from: '+12519298355'
            })
            .then(call => console.log(call));

    }
}

exports.data = methods;


// url: 'http://demo.twilio.com/docs/voice.xml',
// url: 'http://192.168.0.130:8080/public/voice.xml',