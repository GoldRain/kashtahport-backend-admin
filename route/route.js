var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

//add schema model 
var user = require('../model/user');
var hotel = require('../model/hotel');
var booking = require('../model/booking');
var feedback = require('../model/feedback');
var otp = require('../model/otp');
var activity = require('../model/activity');
var scheduleBooking = require('../model/scheduleBooking');
var commonMethodes = require('./commonMethodes');
var paymentHistory = require('../model/paymentHistory');
var notification = require('../notification/notify');
var http = require('http');
var bodyParser = require('body-parser');
var authentication = require('./authenticate');
var path = require('path');
var nodemailer = require('nodemailer');
var schedule = require('node-schedule');
//var country = require('countryjs');


const LanguageDetect = require('languagedetect');
const lngDetector = new LanguageDetect();
//import translate from 'translate';
const translate = require('translate');
var googleTranslate = require('google-translate')('AIzaSyAgOG1E-tCFQvrGSP1ZOx4MjamFZRQf4q8');



//connects with admin
admin = require('../admin/api')

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};


module.exports = function (app) {

    app.get('/', function (req, res) {
        res.json({ error: false, message: "Hotel api is working." })
    })

    app.post('/userInfo', function (req, res) {
        console.log("user Info is Here ");
    })

    app.post('/login', function (req, res) {
        // if (authentication.data.authenticateKey(req, res)) {
        var emaill = req.body.email;
        var passwordd = req.body.password;

        console.log("request body is ", emaill, ">>>>>", passwordd);
        user.findOne({ "email": emaill }, function (err, userData) {
            if (err) {
                console.log("Error >>>>>", err);
            } else {
                if (userData) {
                    console.log("true")
                    user.findOne({ "email": emaill, "password": passwordd }, function (err, userData1) {
                        if (err) {
                            console.log("Error >>>>>", err);
                        } else {
                            if (userData1) {
                                user.updateOne({ "email": emaill, "password": passwordd }, { $set: { is_login: true } }, function (err, data) {
                                    if (err) {
                                        console.log("error>>", err);
                                    } else {
                                        if (data) {
                                            user.findOne({ "email": emaill, "password": passwordd }, function (err, userDataa) {
                                                if (err) {
                                                    console.log("Error >>>>>", err);
                                                } else {
                                                    // if(userDataa){
                                                    res.json({ "error": false, "message": "user data", "userData": userDataa });
                                                    // }
                                                }
                                            })
                                        }
                                    }
                                })
                            } else {
                                res.json({ "error": true, "message": "Wrong password" });
                            }
                        }
                    })
                } else {
                    res.json({ "error": true, "message": "Your email is not registered" });
                }
            }
        })
        // }
    })

    app.post('/logout', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var email = req.body.email;
            var password = req.body.password;

            user.findOne({ email: email }, function (err, userData) {
                if (err) {
                    console.log("Error >>>>>", err);
                } else {
                    if (userData) {
                        user.updateOne({ email: email }, { $set: { is_login: false, device_type: "", notification_token: "" } }, function (err, data) {
                            if (err) {
                                console.log("error>>", err);
                            } else {
                                if (data) {
                                    user.findOne({ email: email }, function (err, userDataa) {
                                        if (err) {
                                            console.log("Error >>>>>", err);
                                        } else {
                                            if (userDataa) {
                                                res.json({ "error": false, "message": "user logout", "userData": userDataa });
                                            }
                                        }
                                    })
                                }
                            }
                        })
                    } else {
                        res.json({ "error": true, "message": "User not found" });
                    }
                }
            })
        }
    })

    app.post('/registerUser', function (req, res) {
        console.log("request body is here>>>>>>>>>>>>", req);
        if (authentication.data.authenticateKey(req, res)) {
            var emaill = req.body.email;
            user.findOne({ email: emaill }, function (err, dataa) {
                if (err) {

                } else {
                    if (dataa) {
                        res.json({ "error": true, "message": "Email already exists" });
                    } else {
                        let name = req.body.name;
                        let email = req.body.email;
                        let phone = req.body.phone;
                        // let type = req.body.type;
                        let password = req.body.password;
                        let profile_image_url = req.body.profile_image_url;
                        let countryCode = req.body.country_code;
                        let facebookId = req.body.facebook_id;

                        var usr = new user({
                            name: name,
                            email: email,
                            phone: phone,
                            //type: type,
                            password: password,
                            profile_image_url: profile_image_url,
                            country_code: countryCode,
                            facebook: facebookId
                        });

                        // save user details
                        usr.save(function (err, data) {
                            if (err) {
                                res.json({ "error": true, "message": "Database Error! save user details", "err": err });
                            } else {
                                if (data) {
                                    user.findOne({ _id: ObjectId(data._id) }, function (err, finalData) {
                                        if (err) {
                                            res.json({ "error": true, "message": "user not register" });
                                        } else {
                                            res.json({ "error": false, "message": "You have successfully register", "data": finalData });
                                        }
                                    })
                                } else {
                                    res.json({ "error": true, "message": "Something went wrong" });
                                }
                            }
                        })
                    }
                }
            })
        }
    })

    app.post('/addProperty', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            console.log("Request body is >>>>>>>>", req.body);

            googleTranslate.translate([req.body.country, req.body.city, req.body.address], 'en', function (err, translation) {
                if (err) {
                    console.log("Error>>>>>>>>>>", err);
                } else {
                    console.log("Translated in english>>>>>>>>>>", translation);
                    req.body.country = translation[0].translatedText;
                    req.body.city = translation[1].translatedText;
                    req.body.address = translation[2].translatedText;

                    var name = req.body.name;
                    var type = req.body.type;
                    var city = req.body.city;
                    var country = req.body.country;
                    var rooms = req.body.rooms_number;
                    var wc_number = req.body.wc_number;
                    var person = req.body.person_number;
                    var dates = req.body.available_date;
                    var ownerId = req.body.owner_id;
                    var rate = req.body.rate;
                    var children = req.body.children;
                    var isEnable = req.body.is_enable;
                    var amenities = req.body.amenities;
                    var longitude = parseFloat(req.body.longitude);
                    var latitude = parseFloat(req.body.latitude);
                    var description = req.body.description;
                    var address = req.body.address;
                    var contactInfo = req.body.contact_info;
                    var currency = req.body.currency;

                    var location = [];
                    var pics = [];
                    var condi = [];
                    //var amn = [];

                    if (req.body.pictures) {
                        let pictures = req.body.pictures.toString();
                        pictures.split(",").forEach(pic => {
                            pics.push(pic);
                        });
                    }
                    // if (req.body.amenities) {
                    //     let amenities = req.body.amenities.toString();
                    //     amenities.split(",").forEach(amnt => {
                    //         amn.push(amnt);
                    //     })
                    // }
                    if (req.body.conditions) {
                        let conditions = req.body.conditions.toString();
                        conditions.split(",").forEach(con => {
                            condi.push(con);
                        })
                    }

                    location.push(longitude);
                    location.push(latitude);
                    //location = [longitude,latitude ];

                    var htel = new hotel({
                        owner_id: ObjectId(ownerId),
                        type: type,
                        name: name,
                        city: city,
                        country: country,
                        pictures: pics,
                        rooms_number: rooms,
                        wc_number: wc_number,
                        person_number: person,
                        amenities: amenities,
                        conditions: condi,
                        available_date: dates,
                        rate: rate,
                        children: children,
                        is_enable: isEnable,
                        longitude: longitude,
                        latitude: latitude,
                        "location.coordinates": location,
                        description: description,
                        address: address,
                        contact_info: contactInfo,
                        currency: currency,
                    });

                    htel.save(function (err, data) {
                        if (err) {
                            console.log("error>>>>>>>>>", err);
                        } else {
                            if (data) {
                                hotel.findOne({ _id: ObjectId(data._id) }, function (err, finalData) {
                                    if (err) {
                                        res.json({ error: true, "message": "Something went wrong" });
                                    } else {
                                        user.updateOne({ _id: ObjectId(ownerId) }, { $set: { can_sale: true } }, function (err, sale) {
                                            if (err) {

                                            } else {
                                                console.log("Can sale True");
                                            }
                                        })
                                        console.log("Final data is here>>>>>>>>", finalData);
                                        res.json({ error: false, "message": "Property added successfully", "data": finalData });
                                    }
                                })
                            } else {
                                res.json({ error: true, "message": "Something went wrong" });
                            }
                        }
                    })
                }
            });
        }
    })

    app.post('/listProperty', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var userId = req.body.user_id;

            hotel.find({ owner_id: ObjectId(userId), is_deleted: false }, function (err, data) {
                if (err) {
                    console.log("error>>>>", err);
                } else {
                    if (data) {
                        res.json({ error: false, "message": "List of Property", "data": data });
                    } else {
                        res.json({ error: true, "message": "No property found" });
                    }
                }
            })
        }
    })

    app.post('/singleProperty', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var hotelId = req.body.hotel_id;

            feedback.find({ hotel_id: ObjectId(hotelId) }, function (err, feedbackData) {
                if (err) {
                    console.log("Error in finding feedback data");
                } else {
                    if (feedbackData.length > 0) {
                        hotel.aggregate([{ $match: { _id: ObjectId(hotelId) } }, { $lookup: { from: 'feedbacks', localField: '_id', foreignField: 'hotel_id', as: 'feedbackData' } }
                            , { $unwind: '$feedbackData' }, {
                            $group: {
                                "_id": "",
                                avgCommunication: { $avg: "$feedbackData.communication" },
                                avgProficiency: { $avg: "$feedbackData.proficiency" },
                                avgPrice: { $avg: "$feedbackData.price" },
                                avgDelivery: { $avg: "$feedbackData.delivery" },
                                "hotelData": { "$push": "$$ROOT" }
                            }
                        }
                        ], function (err, data) {
                            if (err) {
                                console.log("error>>>>", err);
                            } else {
                                if (data) {
                                    res.json({ error: false, "message": "Property", "data": data });
                                } else {
                                    res.json({ error: true, "message": "No property found" });
                                }
                            }
                        })
                    }
                    else {
                        hotel.aggregate([{ $match: { _id: ObjectId(hotelId) } }, { $lookup: { from: 'feedbacks', localField: '_id', foreignField: 'hotel_id', as: 'feedbackData' } }
                            // , { $unwind: '$feedbackData' }, {
                            //     $group: {
                            //         "_id": "",
                            //         avgCommunication: { $avg: "$feedbackData.communication" },
                            //         avgProficiency: { $avg: "$feedbackData.proficiency" },
                            //         avgPrice: { $avg: "$feedbackData.price" },
                            //         avgDelivery: { $avg: "$feedbackData.delivery" },
                            //         "hotelData": { "$push": "$$ROOT" }
                            //     }
                            // }
                        ], function (err, data) {
                            if (err) {
                                console.log("error>>>>", err);
                            } else {
                                if (data) {
                                    res.json({ error: false, "message": "Property", "data": data });
                                } else {
                                    res.json({ error: true, "message": "No property found" });
                                }
                            }
                        })
                    }
                }
            })
        }
    })
    //helo mister saini
    app.post('/manageProperty', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var holetId = req.body.hotel_id;
            var isEnable = req.body.is_enable;
            var currentDate = Date.now().toString();

            console.log("Starting", typeof currentDate);
            if (isEnable == "false") {
                booking.aggregate([{ $match: { $and: [{ hotel_id: ObjectId(holetId) }, { date_to: { $gt: currentDate } }, { status: { $ne: "Cancel" } }, { status: { $ne: "Reject" } }] } }], function (error, bookingData) {
                    if (error) {
                        console.log("Error aa gya", error)
                    } else {
                        console.log("Error nhi aaya", bookingData)
                        if (bookingData.length > 0) {
                            console.log("booking Data aaya", bookingData);
                            res.json({ error: true, "message": "You have active booking request!" });
                        } else {
                            console.log("booking Error nhi aaya");
                            console.log("type of isEnable ", typeof isEnable);
                            hotel.updateOne({ _id: ObjectId(holetId) }, { $set: { is_enable: isEnable } }, function (err, data) {
                                if (err) {
                                    console.log("error>>>>>>>>>>", err);
                                } else {
                                    if (data) {
                                        res.json({ error: false, "message": "status updated successfully" });
                                    } else {
                                        res.json({ error: true, "message": "something went wrong" });
                                    }
                                }
                            })
                        }
                    }
                })
            } else {
                hotel.updateOne({ _id: ObjectId(holetId) }, { $set: { is_enable: isEnable } }, function (err, data) {
                    if (err) {
                        console.log("error>>>>>>>>>>", err);
                    } else {
                        if (data) {
                            res.json({ error: false, "message": "status updated successfully" });
                        } else {
                            res.json({ error: true, "message": "something went wrong" });
                        }
                    }
                })
            }
        }
    })

    app.post('/bookingRequest', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var userId = req.body.user_id;

            booking.aggregate([{ $match: { owner_id: ObjectId(userId) } },
            { $lookup: { from: 'users', localField: 'buyer_id', foreignField: '_id', as: 'buyerData' } }, { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } }], function (err, bookingList) {
                if (err) {
                    console.log("error", err);
                } else {
                    if (bookingList.length > 0) {
                        res.json({ "error": false, "message": "Booking List", "data": bookingList });
                    } else {
                        res.json({ error: true, message: "No data found" });
                    }
                }
            })
        }
    })

    app.post('/updateBooking', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var bookingId = req.body.booking_id;
            var status = req.body.status;

            booking.findOneAndUpdate({ _id: ObjectId(bookingId) }, { $set: { status: status } }, function (err, data) {
                if (err) {
                    console.log("Error>>>>>>", err);
                } else {
                    if (data) {
                        res.json({ error: false, "message": "Booking status Updated" });
                        //console.log("Booking data is >>>>>>>>>>>>>>>>",data);

                        if (status == "Paid") {

                            user.findOne({ _id: ObjectId(data.owner_id) }, function (err, sale) {
                                if (err) {

                                } else {
                                    // console.log("owner5 data at createbooking is >>>>>>>>", sale,"bookingId>>>>>>>>>>>>>",data._id);
                                    if (sale.notification_enable) {
                                        let obj = { "message": "The amount has been paid", "data": data._id, "type": "Paid" };
                                        if (sale.device_type == "Android") {
                                            notification.sendNotification([sale.notification_token], obj, data.owner_id, function (data1) {

                                            });
                                        } else {
                                            console.log("IOS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                            notification.sendNotificationForIOS([sale.notification_token], obj, "The amount has been paid", "Paid", "Amount paid", data.owner_id, function (data1) {

                                            });
                                        }
                                    }

                                    commonMethodes.data.updateActivity(data.buyer_id, "You paid for a booking", "Book_Buyer", bookingId, data.hotel_id, function (result) {
                                        if (result == true) {
                                            console.log("Activity Saved for Buyer");
                                        }
                                    });
                                    commonMethodes.data.updateActivity(data.owner_id, "The amount has been paid", "Book_Seller", bookingId, data.hotel_id, function (result) {
                                        if (result == true) {
                                            console.log("Activity Saved for Seller");
                                        }
                                    });
                                    // commonMethodes.data.updateActivity(data.owner_id, "The amount has been paid", "Paid", data._id, data.hotel_id, function (result) {
                                    //     if (result == true) {
                                    //         console.log("Activity Saved for Seller");
                                    //     }
                                    // });
                                }
                            })
                        } else {
                            if (status == "Confirm") {
                                user.findOne({ _id: ObjectId(data.buyer_id) }, function (err, sale) {
                                    if (err) {

                                    } else {
                                        // console.log("owner5 data at createbooking is >>>>>>>>", sale,"bookingId>>>>>>>>>>>>>",data._id);
                                        if (sale.notification_enable) {
                                            let obj = { "message": "Your booking has been confirmed", "data": data._id, "type": "Book" };
                                            if (sale.device_type == "Android") {
                                                notification.sendNotification([sale.notification_token], obj, data.buyer_id, function (data1) {

                                                });
                                            } else {
                                                console.log("IOS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                                notification.sendNotificationForIOS([sale.notification_token], obj, "Your booking has been confirmed", "Book", "Booking confirmed", data.buyer_id, function (data1) {

                                                });
                                            }
                                        }

                                        commonMethodes.data.updateActivity(data.buyer_id, "Your booking has been confirmed", "Book_Buyer", bookingId, data.hotel_id, function (result) {
                                            if (result == true) {
                                                console.log("Activity Saved for Buyer");
                                            }
                                        });
                                        commonMethodes.data.updateActivity(data.owner_id, "You have confirmed a booking", "Book_Seller", bookingId, data.hotel_id, function (result) {
                                            if (result == true) {
                                                console.log("Activity Saved for Seller");
                                            }
                                        });
                                    }
                                })
                            } else {
                                if (status == "Reject") {
                                    user.findOne({ _id: ObjectId(data.buyer_id) }, function (err, sale) {
                                        if (err) {

                                        } else {
                                            // console.log("owner5 data at createbooking is >>>>>>>>", sale,"bookingId>>>>>>>>>>>>>",data._id);
                                            console.log("Hotel_Id>>>>>>>>>>>>>>>>>>>", data.hotel_id);
                                            console.log("Buyer>>>>>>>>>>>>>>>>>>>", data.buyer_id);
                                            console.log("owner_Id>>>>>>>>>>>>>>>>>>>", data.owner_id);
                                            if (sale.notification_enable) {
                                                let obj = { "message": "Your booking has been rejected", "data": data._id, "type": "Book" };
                                                if (sale.device_type == "Android") {
                                                    notification.sendNotification([sale.notification_token], obj, data.buyer_id, function (data1) {

                                                    });
                                                } else {
                                                    console.log("IOS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                                    notification.sendNotificationForIOS([sale.notification_token], obj, "Your booking has been rejected", "Book", "Booking rejected", data.buyer_id, function (data1) {

                                                    });
                                                }
                                            }

                                            commonMethodes.data.updateActivity(data.buyer_id, "Your booking has been rejected", "Book_Buyer", bookingId, data.hotel_id, function (result) {
                                                if (result == true) {
                                                    console.log("Activity Saved for Buyer");
                                                }
                                            });
                                            commonMethodes.data.updateActivity(data.owner_id, "You have rejected a booking", "Book_Seller", bookingId, data.hotel_id, function (result) {
                                                if (result == true) {
                                                    console.log("Activity Saved for Seller");
                                                }
                                            });
                                        }
                                    })
                                } else {
                                    if (status == "Cancel") {
                                        user.findOne({ _id: ObjectId(data.owner_id) }, function (err, sale) {
                                            if (err) {

                                            } else {
                                                // console.log("owner5 data at createbooking is >>>>>>>>", sale,"bookingId>>>>>>>>>>>>>",data._id);
                                                if (sale.notification_enable) {
                                                    let obj = { "message": "The booking has been cancelled", "data": data._id, "type": "Book_get" };
                                                    if (sale.device_type == "Android") {
                                                        notification.sendNotification([sale.notification_token], obj, data.owner_id, function (data1) {

                                                        });
                                                    } else {
                                                        console.log("IOS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                                        notification.sendNotificationForIOS([sale.notification_token], obj, "The booking has been cancelled", "Book_get", "Booking cancelled", data.owner_id, function (data1) {

                                                        });
                                                    }
                                                }

                                                commonMethodes.data.updateActivity(data.buyer_id, "You have cancelled a booking", "Book_Buyer", bookingId, data.hotel_id, function (result) {
                                                    if (result == true) {
                                                        console.log("Activity Saved for Buyer");
                                                    }
                                                });
                                                commonMethodes.data.updateActivity(data.owner_id, "The booking has been cancelled", "Book_Seller", bookingId, data.hotel_id, function (result) {
                                                    if (result == true) {
                                                        console.log("Activity Saved for Seller");
                                                    }
                                                });
                                            }
                                        })
                                    }
                                }
                            }
                        }
                    } else {
                        res.json({ error: true, "message": "Something went wrong" });
                    }
                }
            })
        }
    })

    app.post('/singleBookingRequest', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var bookingId = req.body.booking_id;

            booking.aggregate([{ $match: { _id: ObjectId(bookingId) } },
            { $lookup: { from: 'users', localField: 'buyer_id', foreignField: '_id', as: 'buyerData' } }, { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } }], function (err, booking) {
                if (err) {
                    console.log("error", err);
                } else {
                    if (booking.length > 0) {
                        res.json({ "error": false, "message": "Booking", "data": booking });
                    } else {
                        res.json({ error: true, message: "No data found" });
                    }
                }
            })
        }
    })

    app.post('/createBooking', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var ownerId = req.body.owner_id;
            var buyerId = req.body.buyer_id;
            var holetId = req.body.hotel_id;
            var children = req.body.children;
            var adult = req.body.adult;
            var room = req.body.room;
            var dateFrom = req.body.date_from;
            var dateTo = req.body.date_to;
            var price = req.body.price;

            var book = new booking({
                owner_id: ObjectId(ownerId),
                buyer_id: ObjectId(buyerId),
                hotel_id: ObjectId(holetId),
                childrens: children,
                adults: adult,
                rooms: room,
                date_from: dateFrom,
                date_to: dateTo,
                price: price,
            });

            book.save(function (err, data) {
                if (err) {
                    console.log("Error>>>>>>>>", err);
                } else {
                    if (data) {
                        booking.findOne({ _id: ObjectId(data._id) }, function (err, finalData) {
                            if (err) {
                                res.json({ error: true, "message": "Something went wrong" });
                            } else {
                                console.log("Booking Id at the save booking in create booking", data._id);

                                //activity for both sellaer and buyer
                                commonMethodes.data.updateActivity(buyerId, "You have booked a property", "Book_Buyer", data._id, holetId, function (result) {
                                    if (result == true) {
                                        console.log("Activity Saved for Buyer");
                                    }
                                });
                                commonMethodes.data.updateActivity(ownerId, "You have got a new booking", "Book_Seller", data._id, holetId, function (result) {
                                    if (result == true) {
                                        console.log("Activity Saved for Seller");
                                    }
                                });

                                res.json({ error: false, "message": "Booking request sent, you will notified when your booking accepted", "data": finalData });

                                //Notification for seller
                                user.findOne({ _id: ObjectId(ownerId) }, function (err, sale) {
                                    if (err) {

                                    } else {

                                        console.log("owner5 data at createbooking is >>>>>>>>", sale, "bookingId>>>>>>>>>>>>>", data._id);
                                        if (sale.notification_enable) {
                                            let obj = { "message": "You have got a new booking", "data": data._id, "type": "Book_Get" };
                                            if (sale.device_type == "Android") {
                                                notification.sendNotification([sale.notification_token], obj, ownerId, function (data1) {

                                                });
                                            } else {
                                                console.log("IOS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                                notification.sendNotificationForIOS([sale.notification_token], obj, "You got a booking", "Booking", "New booking request", ownerId, function (data1) {

                                                });
                                            }
                                        }
                                    }
                                })
                            }
                        })

                        //notification for the buyer when bokking is comming close>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        let myDate = parseInt(dateFrom)
                        console.log("myDate is >>>>>>>>>>>", myDate, ">>>>>>>>>>>>>", typeof myDate);
                        let scheduleTime = myDate - 3600000;

                        console.log("Schedule time is >>>>>>>>>>>", scheduleTime, ">>>>>>>>>>", typeof scheduleTime);
                        // let date = new Date(parseInt(scheduleTime));
                        // let date = new Date(scheduleTime);

                        let schedule1 = new scheduleBooking({
                            owner_id: ObjectId(ownerId),
                            buyer_id: ObjectId(buyerId),
                            hotel_id: ObjectId(holetId),
                            booking_id: ObjectId(data._id),
                            created_at: Date.now(),
                            scheduled_at: scheduleTime,
                        })

                        schedule1.save((err, scheduleSave) => {
                            if (err) {
                                console.log("schedule message save ", err);
                            }
                            else {
                                if (scheduleSave) {
                                    //console.log("save id is ", scheduleMessageSave._id);
                                    console.log("save date is ", scheduleTime);

                                    var j = schedule.scheduleJob(scheduleTime, function () {
                                        console.log("Schedule job was runs perfectly at>>>>>>>>", scheduleTime, ">>>>>>>>", data._id);
                                        booking.aggregate([{ $match: { _id: ObjectId(data._id) } }, { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } }], function (err, bookingData) {
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                console.log("booking data at createbooking is >>>>>>>>", bookingData);
                                                if (bookingData.status == "Confirm" || bookingData.status == "Paid") {
                                                    user.findOne({ _id: ObjectId(buyerId) }, function (err, sale) {
                                                        if (err) {

                                                        } else {
                                                            console.log("user data at createbooking is >>>>>>>>", sale);
                                                            if (sale.notification_enable) {
                                                                let obj = { "message": "Your booking is coming close", "data": data._id, "type": "Book" };
                                                                if (sale.device_type == "Android") {
                                                                    notification.sendNotification([sale.notification_token], obj, buyerId, function (data1) {
                                                                        if (data1) {
                                                                            commonMethodes.data.updateActivity(buyerId, "Your booking is coming close", "Book_Buyer", data._id, holetId, function (result) {
                                                                                if (result == true) {
                                                                                    console.log("Activity Saved for Buyer");
                                                                                }
                                                                            });
                                                                        }
                                                                    });


                                                                } else {
                                                                    notification.sendNotificationForIOS([sale.notification_token], obj, "Your booking is coming close", "Booking", "Booking", buyerId, function (data1) {

                                                                        if (data1) {
                                                                            commonMethodes.data.updateActivity(buyerId, "Your booking is coming close", "Book_Buyer", data._id, holetId, function (result) {
                                                                                if (result == true) {
                                                                                    console.log("Activity Saved for Buyer");
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    })
                                                } else {
                                                    console.log("Notification not sent");
                                                }
                                            }
                                        })

                                    });
                                }
                                else {
                                    console.log("something went wrong");
                                }
                            }
                        })

                        //notification for the buyer when bokking is end >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        let myDate1 = parseInt(dateTo)
                        console.log("myDate1 is >>>>>>>>>>>", myDate1, ">>>>>>>>>>>>>", typeof myDate1);
                        let scheduleTime1 = myDate1 - 3600000;

                        console.log("Schedule1 time is >>>>>>>>>>>", scheduleTime1, ">>>>>>>>>>", typeof scheduleTime1);
                        // let date = new Date(parseInt(scheduleTime));
                        // let date = new Date(scheduleTime);

                        let schedule11 = new scheduleBooking({
                            owner_id: ObjectId(ownerId),
                            buyer_id: ObjectId(buyerId),
                            hotel_id: ObjectId(holetId),
                            booking_id: ObjectId(data._id),
                            created_at: Date.now(),
                            scheduled_at: scheduleTime1,
                        })

                        schedule11.save((err, scheduleSave1) => {
                            if (err) {
                                console.log("schedule message save ", err);
                            }
                            else {
                                if (scheduleSave1) {
                                    //console.log("save id is ", scheduleMessageSave._id);
                                    console.log("save date1 is ", scheduleTime1);

                                    var j = schedule.scheduleJob(scheduleTime1, function () {
                                        console.log("Schedule job was runs perfectly at111>>>>>>>>", scheduleTime1, ">>>>>>>>", data._id);
                                        booking.aggregate([{ $match: { _id: ObjectId(data._id) } }, { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } }], function (err, bookingData1) {
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                console.log("booking data1111 at createbooking is >>>>>>>>", bookingData1);
                                                if (bookingData1) {
                                                    user.findOne({ _id: ObjectId(buyerId) }, function (err, sale) {
                                                        if (err) {

                                                        } else {
                                                            console.log("user data at createbooking is >>>>>>>>", sale);
                                                            if (sale.notification_enable) {
                                                                let obj = { "message": "Your booking is completed soon", "data": data._id, "type": "Book" };
                                                                if (sale.device_type == "Android") {
                                                                    notification.sendNotification([sale.notification_token], obj, buyerId, function (data1) {
                                                                        if (data1) {
                                                                            commonMethodes.data.updateActivity(buyerId, "Your booking is completed soon", "Book_Buyer", data._id, holetId, function (result) {
                                                                                if (result == true) {
                                                                                    console.log("Activity Saved for Buyer");
                                                                                }
                                                                            });
                                                                        }
                                                                    });


                                                                } else {
                                                                    notification.sendNotificationForIOS([sale.notification_token], obj, "Your booking is completed soon", "Booking", "Booking", buyerId, function (data1) {

                                                                        if (data1) {
                                                                            commonMethodes.data.updateActivity(buyerId, "Your booking is completed soon", "Book_Buyer", data._id, holetId, function (result) {
                                                                                if (result == true) {
                                                                                    console.log("Activity Saved for Buyer");
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    })
                                                } else {
                                                    console.log("Notification not sent111");
                                                }
                                            }
                                        })

                                    });
                                }
                                else {
                                    console.log("something went wrong111");
                                }
                            }
                        })
                    } else {
                        res.json({ error: true, "message": "Something went wrong" });
                    }
                }
            })
        }
    })

    app.post('/updateProperty', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            // console.log("body>>>>>>>>>>", req.body);
            var hotelId = req.body.hotel_id;
            var date = Date.now();

            var pics = [];
            var condi = [];
            var location = [];

            var longitude = parseFloat(req.body.longitude);
            var latitude = parseFloat(req.body.latitude);
            location.push(longitude);
            location.push(latitude);

            if (req.body.pictures) {
                let pictures = req.body.pictures.toString();
                pictures.split(",").forEach(pic => {
                    pics.push(pic);
                });
            }

            if (req.body.conditions) {
                let conditions = req.body.conditions.toString();
                conditions.split(",").forEach(con => {
                    condi.push(con);
                })
            }

            req.body.pictures = pics;
            req.body.location = {
                type: "Point",
                coordinates: location
            };
            req.body.conditions = condi;
            req.body.updated_at = date;

            console.log("Req.Body for update property is ", req.body);
            hotel.findOneAndUpdate({ _id: ObjectId(hotelId) }, { $set: req.body }, function (err, data) {
                if (err) {
                    console.log("Error>>>>>>>", err);
                } else {
                    if (data) {
                        res.json({ error: false, "message": "Property updated successfully", "data": data });
                    } else {
                        res.json({ error: true, "message": "Something went wrong" });
                    }
                }
            })
        }
    })

    app.post('/addToFavourite', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var userId = req.body.user_id;
            var hotelId = req.body.hotel_id;
            console.log("userid: ", userId)
            console.log("hotelId: ", hotelId)
            user.findOneAndUpdate({ _id: ObjectId(userId) }, { $push: { favourites: ObjectId(hotelId) } }, function (err, data) {
                if (err) {
                    console.log("Error>>>>>>", err);
                } else {
                    if (data) {
                        res.json({ error: false, "message": "Added to favourites list", "data": data });

                        hotel.findOne({ _id: ObjectId(hotelId) }, function (err, hotelData) {
                            if (err) {
                                console.log("Error>>>>>>>", err);
                            } else {
                                //console.log()
                                // commonMethodes.data.updateActivity(userId, "You have liked a property", "Like", hotelId, hotelId, function (result) {
                                //     if (result == true) {
                                //         console.log("Activity Saved for Buyer");
                                //     }
                                // });
                                commonMethodes.data.updateActivity(hotelData.owner_id, data.name + "@ liked your listing", "Like_Seller", hotelId, hotelId, function (result) {
                                    if (result == true) {
                                        console.log("Activity Saved for Seller");
                                    }
                                });

                                user.findOne({ _id: ObjectId(hotelData.owner_id) }, function (err, sale) {
                                    if (err) {

                                    } else {
                                        //console.log("owner5 data at createbooking is >>>>>>>>", sale,"bookingId>>>>>>>>>>>>>",data._id);
                                        if (sale.notification_enable && hotelData.owner_id != userId) {
                                            let obj = { "message": "Your property has been liked", "data": hotelId, "type": "Like" };
                                            if (sale.device_type == "Android") {
                                                notification.sendNotification([sale.notification_token], obj, hotelData.owner_id, function (data1) {

                                                });
                                            } else {
                                                console.log("IOS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                                notification.sendNotificationForIOS([sale.notification_token], obj, "Your property has been liked", "Like", "Like", hotelData.owner_id, function (data1) {

                                                });
                                            }
                                        }
                                    }
                                })
                            }
                        })
                    } else {
                        console.log("[7]")
                        res.json({ error: true, "message": "Something went wrong" });
                    }
                }
            })
        }
    })

    app.post('/removeToFavourite', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var userId = req.body.user_id;
            var hotelId = req.body.hotel_id;

            user.findOneAndUpdate({ _id: ObjectId(userId) }, { $pull: { favourites: ObjectId(hotelId) } }, function (err, data) {
                if (err) {
                    console.log("Error>>>>>>", err);
                } else {
                    if (data) {
                        res.json({ error: false, "message": "Removed from favourites list", "data": data });
                    } else {
                        res.json({ error: true, "message": "Something went wrong" });
                    }
                }
            })
        }
    })

    app.post('/FavouriteHotelList', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var userId = req.body.user_id;
            console.log("Fav");
            user.findOne({ _id: ObjectId(userId) }, function (err, data) {
                if (err) {
                    console.log("Error>>>>>", err);
                } else {
                    console.log("user found>>>>>>>", data);
                    if (data) {
                        let fav = data.favourites;
                        let favs = [];
                        if (fav.length == 0) {
                            res.json({ error: true, message: "No data found" });
                        }
                        console.log("favourite hotel found>>>>>>>", fav);
                        for (let i = 0; i < fav.length; i++) {
                            console.log("in for loop");
                            favs[i] = ObjectId(fav[i]);

                            if (i == fav.length - 1) {
                                hotel.aggregate([{ $match: { _id: { $in: favs } } },
                                { $lookup: { from: 'users', localField: 'owner_id', foreignField: '_id', as: 'ownerData' } }], function (err, favouriteList) {
                                    if (err) {
                                        console.log("error", err);
                                    } else {
                                        console.log("List>>>", favouriteList);
                                        if (favouriteList.length > 0) {
                                            res.json({ "error": false, "message": "Hotel List", "data": favouriteList });
                                        } else {
                                            console.log("No favourite hotel found>>>>>>");
                                            res.json({ error: true, message: "No data found" });
                                        }
                                    }
                                })
                            }
                        }
                    } else {
                        res.json({ error: true, "message": "user not found" });
                    }
                }
            })
        }
    })

    app.post('/searchHotel', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {

            googleTranslate.translate([req.body.country, req.body.city], 'en', function (err, translation) {
                if (err) {
                    console.log("Error>>>>>>>>>>", err);
                } else {
                    console.log("Translated in english>>>>>>>>>>", translation);
                    req.body.country = translation[0].translatedText;
                    req.body.city = translation[1].translatedText;


                    var obj = {};
                    var obj1 = {};
                    var obj2 = { is_enable: true, is_deleted: false, is_approved: true };
                    if (req.body.type) {
                        if (req.body.type == "all") {
                            console.log("All type of hotel");
                        } else {
                            obj.type = req.body.type;
                        }
                    }
                    if (req.body.country) {
                        obj.country = { $regex: ".*" + req.body.country + ".*", $options: "i" };
                    }
                    if (req.body.city) {
                        obj.city = { $regex: ".*" + req.body.city + ".*", $options: "i" };
                    }
                    if (req.body.rate) {
                        obj.rate = { $lte: req.body.rate }
                    }
                    if (req.body.rooms) {
                        obj.rooms_number = { $gte: req.body.rooms }
                    }
                    if (req.body.children) {
                        obj.children = { $gte: req.body.children }
                    }
                    if (req.body.person) {
                        obj.person_number = { $gte: req.body.person }
                    }
                    if (req.body.wc_number) {
                        obj.wc_number = { $gte: req.body.wc_number }
                    }
                    if (req.body.rating) {
                        obj.rating = req.body.rating;
                    }
                    if (req.body.date_from && req.body.date_to) {
                        obj1 = { "available_date.start_date": { $lte: req.body.date_from }, "available_date.end_date": { $gte: req.body.date_to } };
                    }

                    console.log("object is here", obj);
                    if (Object.keys(obj).length == 0) {
                        console.log("IF obj is Empty", obj1, ">>>>>>>>>>>>", obj)
                        hotel.aggregate([{ $match: { $and: [obj1, obj2] } }], function (err, hotelList) {
                            if (err) {
                                console.log("error", err);
                            } else {
                                console.log(" List>>>", hotelList);
                                if (hotelList.length > 0) {
                                    res.json({ "error": false, "message": "Hotel List", "data": hotelList });
                                } else {
                                    res.json({ error: true, message: "No data found" });
                                }
                            }
                        })
                    } else {
                        console.log("IF obj is not Empty", obj1, ">>>>>>>>>>>>", obj)
                        hotel.aggregate([{ $match: { $and: [obj, obj1, obj2] } }], function (err, hotelList) {
                            if (err) {
                                console.log("error", err);
                            } else {
                                // console.log(" List>>>", hotelList);
                                if (hotelList.length > 0) {
                                    res.json({ "error": false, "message": "Hotel List", "data": hotelList });
                                } else {
                                    // logic
                                    //res.json({ error: true, message: "No data found" });

                                    var tobj = {};
                                    var tobj1 = {};
                                    var tobj2 = { is_enable: true, is_deleted: false, is_approved: true };
                                    if (req.body.type) {
                                        if (req.body.type == "all") {
                                            console.log("All type of hotel");
                                        } else {
                                            tobj.type = req.body.type;
                                        }
                                    }
                                    if (req.body.country) {
                                        //obj.country = { $regex: ".*" + req.body.country + ".*", $options: "i" };
                                    }
                                    if (req.body.city) {
                                        tobj.country = { $regex: ".*" + req.body.city + ".*", $options: "i" };
                                    }
                                    if (req.body.rate) {
                                        tobj.rate = { $lte: req.body.rate }
                                    }
                                    if (req.body.rooms) {
                                        tobj.rooms_number = { $gte: req.body.rooms }
                                    }
                                    if (req.body.children) {
                                        tobj.children = { $gte: req.body.children }
                                    }
                                    if (req.body.person) {
                                        tobj.person_number = { $gte: req.body.person }
                                    }
                                    if (req.body.wc_number) {
                                        tobj.wc_number = { $gte: req.body.wc_number }
                                    }
                                    if (req.body.rating) {
                                        tobj.rating = req.body.rating;
                                    }
                                    if (req.body.date_from && req.body.date_to) {
                                        tobj1 = { "available_date.start_date": { $lte: req.body.date_from }, "available_date.end_date": { $gte: req.body.date_to } };
                                    }

                                    if (Object.keys(tobj).length == 0) {
                                        console.log("IF obj is Empty 124", tobj1, ">>>>>>>>>>>>", tobj)
                                        hotel.aggregate([{ $match: { $and: [tobj1, tobj2] } }], function (err, hotelList) {
                                            if (err) {
                                                console.log("error", err);
                                            } else {
                                                if (hotelList.length > 0) {
                                                    res.json({ "error": false, "message": "Hotel List", "data": hotelList });
                                                } else {
                                                    res.json({ error: true, message: "No data found" });
                                                }
                                            }
                                        })
                                    } else {
                                        console.log("IF obj is not Empty 123", tobj1, ">>>>>>>>>>>>", tobj)
                                        hotel.aggregate([{ $match: { $and: [tobj, tobj1, tobj2] } }], function (err, hotelList) {
                                            if (err) {
                                                console.log("error", err);
                                            } else {
                                                if (hotelList.length > 0) {
                                                    res.json({ "error": false, "message": "Hotel List", "data": hotelList });
                                                } else {
                                                    // logic
                                                    res.json({ error: true, message: "No data found" });
                                                }
                                            }
                                        })
                                    }

                                }
                            }
                        })
                    }


                }
            });
        }
    })

    app.post('/nearByHotel', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var longitude = req.body.longitude;
            var latitude = req.body.latitude;
            hotel.find({ location: { $near: { $geometry: { type: "Point", coordinates: [longitude, latitude] }, $maxDistance: 1609 } }, is_deleted: false, is_enable: true, is_approved: true }, function (err, data) {
                if (err) {
                    console.log("error>>>>>", err);
                } else {
                    if (data) {
                        console.log("Data is here>>>>>", data);
                        res.json({ error: false, "message": "Near by hotels", "data": data });
                    } else {
                        res.json({ error: true, "message": "Hotels not found" });
                    }
                }
            })
        }
        //db.getCollection('hotels').createIndex({location:"2dsphere"})
        //for search $nearSphere in your database     
    })

    app.post('/viewProfile', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var userId = req.body.user_id;
            let number = req.body.number;
            let query = number * 5;

            // if (req.body.type == "buyer") {
            booking.aggregate([{ $match: { buyer_id: ObjectId(userId) } },
            { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } }, { $sort: { created_at: -1 } }, {
                "$skip": query
            }, {
                "$limit": 5
            }], function (err, List) {
                if (err) {
                    console.log("error", err);
                } else {
                    if (List.length > 0) {
                        res.json({ "error": false, "message": "Profile", "data": List });
                    } else {
                        res.json({ error: true, message: "No data found" });
                    }
                }
            })
            // } else {
            //     if (req.body.type == "seller") {
            //         user.aggregate([{ $match: { _id: ObjectId(userId) } },
            //         { $lookup: { from: 'hotels', localField: '_id', foreignField: 'owner_id', as: 'hotelData' } }, {
            //             "$skip": query
            //         }, {
            //             "$limit": 15
            //         }], function (err, List) {
            //             if (err) {
            //                 console.log("error", err);
            //             } else {
            //                 if (List.length > 0) {
            //                     res.json({ "error": false, "message": "Profile", "data": List });
            //                 } else {
            //                     res.json({ error: true, message: "No data found" });
            //                 }
            //             }
            //         })
            //     }
            // }
        }
    })

    app.post('/updateProfile', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var userId = req.body.user_id;
            var edit = req.body;

            user.findOneAndUpdate({ _id: ObjectId(userId) }, { $set: edit }, function (err, data) {
                if (err) {
                    console.log("Error>>>>>>>", err);
                } else {
                    if (data) {
                        user.findOne({ _id: ObjectId(userId) }, function (err, finalData) {
                            if (err) {

                            } else {
                                if (finalData) {
                                    res.json({ error: false, "message": "Profile updated successfully", "data": finalData });
                                }
                            }
                        })
                    } else {
                        res.json({ error: true, "message": "Something went wrong" });
                    }
                }
            })
        }
    })

    app.post('/feedback', function (req, res) {
        if (authentication.data.authenticateKey(req, res)) {
            var userId = req.body.user_id;
            var hotelId = req.body.hotel_id;
            var communication = req.body.communication;
            var price = req.body.price;
            var delivery = req.body.delivery;
            var proficiency = req.body.proficiency;
            var comment = req.body.comment;
            var bookingId = req.body.booking_id;

            var feed = new feedback({
                user_id: ObjectId(userId),
                hotel_id: ObjectId(hotelId),
                booking_id: ObjectId(bookingId),
                communication: communication,
                price: price,
                delivery: delivery,
                proficiency: proficiency,
                comment: comment,
            });

            feed.save(function (err, data) {
                if (err) {
                    console.log("error<<<<<<<", err);
                } else {
                    if (data) {
                        res.json({ error: false, "message": "Feedback provided successfully" });


                        booking.updateOne({ _id: ObjectId(bookingId) }, { $set: { feedback_given: true } }, function (err, feedbackGiven) {
                            if (err) {
                                console.log("error>>>>>>>>>>>", err);
                            } else {
                                if (feedbackGiven) {
                                    console.log("feedBack is given to the hotel");
                                }
                            }
                        })

                        feedback.aggregate([{ $match: { hotel_id: ObjectId(hotelId) } },
                        {
                            $group:
                            {
                                _id: "",
                                avgCommunication: { $avg: "$communication" },
                                avgPrice: { $avg: "$price" },
                                avgDelivery: { $avg: "$delivery" },
                                avgProficiency: { $avg: "$proficiency" }
                            }
                        }
                        ], function (err, avgData) {
                            if (err) {
                                console.log("Error<<<<<<<<<".err);
                            } else {
                                console.log("data is here", avgData);
                                let com = parseFloat(avgData[0].avgCommunication);
                                let pri = parseFloat(avgData[0].avgPrice);
                                let deli = parseFloat(avgData[0].avgDelivery);
                                let pro = parseFloat(avgData[0].avgProficiency);

                                var rating1 = parseInt((com + pri + deli + pro) / 4);
                                console.log("rating is here", rating1);
                                hotel.findOneAndUpdate({ _id: ObjectId(hotelId) }, { $set: { rating: rating1 } }, function (err, done) {
                                    if (err) {
                                        console.log("Error>>>>>>>>>>", err);
                                    } else {
                                        console.log("Done>>>>>>>>>>", done);

                                        user.findOne({ _id: ObjectId(userId) }, function (err, userData) {
                                            if (err) {
                                                console.log("Error>>>>>>>", err);
                                            } else {
                                                //console.log()
                                                commonMethodes.data.updateActivity(userId, "You gives feedback to a property", "Feedback_Buyer", data._id, hotelId, function (result) {
                                                    if (result == true) {
                                                        console.log("Activity Saved for Buyer");
                                                    }
                                                });
                                                commonMethodes.data.updateActivity(done.owner_id, userData.name + "@ gives feedback to your listing", "Feedback_Seller", data._id, hotelId, function (result) {
                                                    if (result == true) {
                                                        console.log("Activity Saved for Seller");
                                                    }
                                                });

                                                user.findOne({ _id: ObjectId(done.owner_id) }, function (err, sale) {
                                                    if (err) {

                                                    } else {
                                                        // console.log("owner5 data at createbooking is >>>>>>>>", sale,"bookingId>>>>>>>>>>>>>",data._id);
                                                        if (sale.notification_enable) {
                                                            let obj = { "message": userData.name + "@ gives feedback to your listing", "data": bookingId, "type": "Feedback" };
                                                            if (sale.device_type == "Android") {
                                                                notification.sendNotification([sale.notification_token], obj, done.owner_id, function (data1) {

                                                                });
                                                            } else {
                                                                console.log("IOS>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                                                                notification.sendNotificationForIOS([sale.notification_token], obj, userData.name + "@ gives feedback to your listing", "Feedback", "Feedback", done.owner_id, function (data1) {

                                                                });
                                                            }
                                                        }
                                                    }
                                                })
                                            }
                                        });
                                    }
                                })
                            }
                        })
                    } else {
                        res.json({ error: true, "message": "something went wrong" });
                    }
                }
            })
        }
    })

    app.get('/aboutUs', function (req, res) {
        // res.render('aboutus');
        res.sendFile(path.join(__dirname + '/aboutus.html'));
        // http://localhost:8080/aboutUs  sharable link
    })

    app.get('/policy', function (req, res) {
        // res.render('policy');
        res.sendFile(path.join(__dirname + '/policy.html'));
        // http://localhost:8080/policy  sharable link
    })

    app.get('/term', function (req, res) {
        // res.render('policy');
        res.sendFile(path.join(__dirname + '/term.html'));
        // http://localhost:8080/policy  sharable link
    })

    app.post('/getOtp', function (req, res) {
        // if (authentication.data.authenticateKey(req, res)) {
        let phoneNo = req.body.phone;
        let countryCode = req.body.country_code;
        if (phoneNo) {
            commonMethodes.data.send(phoneNo, countryCode, function (data) {
                if (data) {
                    // console.log("data>>>>>>>>>", data);
                    res.json(data)
                } else {
                    res.json({ error: true, message: "Something went wrong, Please try again" });
                    // console.log("Data>>>>>>>>>>");
                }
            });
            // var newOtp = Math.floor(100000 + Math.random() * 900000);
        }
        else {
            res.json({ "error": true, "messagfinalDatae": "Phone number is not in right format" });
        }
        // }
    })

    app.post('/verifyOtp', function (req, res) {
        console.log("Verify otp called");
        if (authentication.data.authenticateKey(req, res)) {
            let phoneNo = req.body.phone;
            let otpText = req.body.otp;
            // var deviceType = "";
            // if (req.body.device_type == undefined)
            //     deviceType = "Android";
            // else
            //     deviceType = req.body.device_type;

            if (phoneNo && otpText) {
                otp.findOne({ phone: phoneNo }, function (err, resOtp) {
                    if (err) {
                        res.json({ "error": true, "message": "Database Error!" });
                    } else {
                        console.log("OTP was found >>>>>>>>>>", resOtp);
                        if (resOtp) {
                            if (resOtp.otp == otpText) {
                                console.log("OTP was matched >>>>>>>>>", Date.now() > (resOtp.expire_at));
                                if (parseInt(resOtp.expire_at) > Date.now()) {
                                    res.json({ "error": false, "message": "OTP verified Successfully", "oldUser": true, });
                                } else {
                                    res.json({ "error": true, "message": "Sorry, this OTP has been expired" });
                                }
                            } else {
                                res.json({ "error": true, "message": "Incorrect OTP, Please try again" });
                            }
                        } else {
                            res.json({ "error": true, "message": "No otp generated for this Number" });
                        }
                    }
                });
            } else {
                if (!phoneNo) {
                    res.json({ "error": true, "message": "Provide Phone parameter" });
                } else {
                    res.json({ "error": true, "message": "Provide otp parameter" });
                }
            }
        }
    })

    app.post('/getActivity', function (req, res) {
        var userId = req.body.user_id;
        var resData = [];

        activity.find({ user_id: ObjectId(userId) },
            function (err, data) {
                if (err) {
                    console.log("error>>>>", err);
                } else {
                    if (data.length > 0) {
                        console.log("ActvityList Found", data.length);
                        let j = data.length;
                        let k = 0;
                        for (let i = 0; i < j; i++) {
                            console.log("Enter the loop");
                            if (data[i].hint == "Book_Buyer") {
                                console.log("Booking Found");
                                activity.aggregate([{ $match: { _id: ObjectId(data[i]._id), hint: "Book_Buyer" } },
                                { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } },
                                { $lookup: { from: 'bookings', localField: 'hint_id', foreignField: '_id', as: 'bookingData' } },
                                ], function (err, finaldata) {
                                    if (err) {
                                        console, log("Error", err);
                                    } else {
                                        if (finaldata) {
                                            console.log("finalData Found", finaldata.length);
                                            resData.push(finaldata);
                                            k++;
                                            if (k == data.length) {
                                                console.log("resData Length", resData.length);
                                                res.json({ error: false, "message": "Activity", "data": resData });
                                            }
                                        } else {
                                            res.json({ error: true, "message": "Activity Not Found" });
                                        }
                                    }
                                })
                            }
                            if (data[i].hint == "Book_Seller") {
                                console.log("Booking Found");
                                activity.aggregate([{ $match: { _id: ObjectId(data[i]._id), hint: "Book_Seller" } },
                                { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } },
                                { $lookup: { from: 'bookings', localField: 'hint_id', foreignField: '_id', as: 'bookingData' } },
                                ], function (err, finaldata) {
                                    if (err) {
                                        console, log("Error", err);
                                    } else {
                                        if (finaldata) {
                                            console.log("finalData Found", finaldata.length);
                                            resData.push(finaldata);
                                            k++;
                                            if (k == data.length) {
                                                console.log("resData Length", resData.length);
                                                res.json({ error: false, "message": "Activity", "data": resData });
                                            }
                                        } else {
                                            res.json({ error: true, "message": "Activity Not Found" });
                                        }
                                    }
                                })
                            }
                            if (data[i].hint == "Like_Seller") {
                                activity.aggregate([{ $match: { _id: ObjectId(data[i]._id), hint: "Like_Seller" } },
                                { $lookup: { from: 'hotels', localField: 'hint_id', foreignField: '_id', as: 'hotelData' } },
                                ], function (err, finaldata) {
                                    if (err) {
                                        console, log("Error", err);
                                    } else {
                                        if (finaldata) {
                                            console.log("finalData Found", finaldata.length);
                                            resData.push(finaldata);
                                            k++;
                                            if (k == data.length) {
                                                console.log("resData Length", resData.length);
                                                res.json({ error: false, "message": "Activity", "data": resData });
                                            }
                                        } else {
                                            res.json({ error: true, "message": "Activity Not Found" });
                                        }
                                    }
                                })
                            }
                            if (data[i].hint == "Feedback_Buyer") {
                                activity.aggregate([{ $match: { _id: ObjectId(data[i]._id), hint: "Feedback_Buyer" } },
                                { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } }, { $lookup: { from: 'feedbacks', localField: 'hint_id', foreignField: '_id', as: 'feedbackData' } },
                                ], function (err, finaldata) {
                                    if (err) {
                                        console, log("Error", err);
                                    } else {
                                        if (finaldata) {
                                            console.log("finalData Found", finaldata.length);
                                            resData.push(finaldata);
                                            k++;
                                            if (k == data.length) {
                                                console.log("resData Length", resData.length);
                                                res.json({ error: false, "message": "Activity", "data": resData });
                                            }
                                        } else {
                                            res.json({ error: true, "message": "Activity Not Found" });
                                        }
                                    }
                                })
                            }
                            if (data[i].hint == "Feedback_Seller") {
                                activity.aggregate([{ $match: { _id: ObjectId(data[i]._id), hint: "Feedback_Seller" } },
                                { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } }, { $lookup: { from: 'feedbacks', localField: 'hint_id', foreignField: '_id', as: 'feedbackData' } },
                                ], function (err, finaldata) {
                                    if (err) {
                                        console, log("Error", err);
                                    } else {
                                        if (finaldata) {
                                            console.log("finalData Found", finaldata.length);
                                            resData.push(finaldata);
                                            k++;
                                            if (k == data.length) {
                                                console.log("resData Length", resData.length);
                                                res.json({ error: false, "message": "Activity", "data": resData });
                                            }
                                        } else {
                                            res.json({ error: true, "message": "Activity Not Found" });
                                        }
                                    }
                                })
                            }
                            if (data[i].hint == "Approved") {
                                activity.aggregate([{ $match: { _id: ObjectId(data[i]._id), hint: "Approved" } },
                                { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } },
                                ], function (err, finaldata) {
                                    if (err) {
                                        console, log("Error", err);
                                    } else {
                                        if (finaldata) {
                                            console.log("finalData Found", finaldata.length);
                                            resData.push(finaldata);
                                            k++;
                                            if (k == data.length) {
                                                console.log("resData Length", resData.length);
                                                res.json({ error: false, "message": "Activity", "data": resData });
                                            }
                                        } else {
                                            res.json({ error: true, "message": "Activity Not Found" });
                                        }
                                    }
                                })
                            }

                        }
                    } else {
                        res.json({ error: true, "message": "Activity Not Found" });
                    }
                }
            })
    })

    app.post('/getSingleActivity', function (req, res) {
        var activityId = req.body.activity_id;

        activity.findOne({ _id: ObjectId(activityId) }, function (err, response) {
            if (err) {
                console.log("error>>>>", err);
            } else {
                if (response) {
                    if (response.hint == "Book") {
                        activity.aggregate([{ $match: { _id: ObjectId(activityId) } },
                        { $lookup: { from: 'hotels', localField: 'hint_id', foreignField: '_id', as: 'hotelData' } },
                        ], function (err, data) {
                            if (err) {
                                console.log("error>>>>", err);
                            } else {
                                if (data) {
                                    res.json({ error: false, "message": "Activity", "data": data });
                                } else {
                                    res.json({ error: true, "message": "Activity Not Found" });
                                }
                            }
                        })
                    }
                    // else if (response.hint == "Consultation") {
                    //     activity.aggregate([{ $match: { _id: ObjectId(activityId) } },
                    //     { $lookup: { from: 'patients', localField: 'patientId', foreignField: '_id', as: 'patientData' } },
                    //     { $lookup: { from: 'consultations', localField: 'hintId', foreignField: '_id', as: 'consultationData' } }, { $project: { patient_message: 0 } }], function (err, data) {
                    //         if (err) {
                    //             console.log("error>>>>", err);
                    //         } else {
                    //             if (data) {
                    //                 res.json({ error: false, "message": "Doctor Activity", "data": data });
                    //             } else {
                    //                 res.json({ error: true, "message": "Doctor Activity Not Found" });
                    //             }
                    //         }
                    //     })
                    // }
                }
            }
        })
    })

    app.post('/viewUserProfile', function (req, res) {
        var userId = req.body.user_id;

        user.findOne({ _id: ObjectId(userId) }, function (err, data) {
            if (err) {
                console.log("Error>>>>>>>>>>>", err);
            } else {
                if (data) {
                    res.json({ error: false, "message": "User data", "data": data });
                } else {
                    res.json({ error: true, "message": "User data not found" });
                }
            }
        })
    })

    app.post('/getFeedbackOfSingleHotel', function (req, res) {
        var hotelId = req.body.hotel_id;

        feedback.aggregate([{ $match: { hotel_id: ObjectId(hotelId) } }, {
            $group: {
                "_id": "",
                avgCommunication: { $avg: "$communication" },
                avgProficiency: { $avg: "$proficiency" },
                avgPrice: { $avg: "$price" },
                avgDelivery: { $avg: "$delivery" },
                "feedbackData": { "$push": "$$ROOT" }
            }
        }], function (err, data) {
            if (err) {
                console.log("error>>>>", err);
            } else {
                if (data) {
                    res.json({ error: false, "message": "Feedback", "data": data });
                } else {
                    res.json({ error: true, "message": "No Feedback found" });
                }
            }
        })
    })

    app.post('/deleteHotel', function (req, res) {
        var hotelId = req.body.hotel_id;

        hotel.findOneAndUpdate({ _id: ObjectId(hotelId) }, { $set: { is_deleted: true } }, function (err, data) {
            if (err) {
                console.log("Error>>>>>>>>>>", err);
            } else {
                if (data) {
                    res.json({ error: false, "message": "Hotel Deleted Successfully" });
                } else {
                    res.json({ error: true, "message": "Something went wrong" });
                }
            }
        })
    })

    app.post('/paymentRequest', function (req, res) {
        var hotelId = req.body.hotel_id;
        var ownerId = req.body.owner_id;
        var buyerId = req.body.buyer_id;
        var bookingId = req.body.booking_id;
        var amount = parseFloat(req.body.amount);
        var paymentType = req.body.payment_type;
        var transactionId = req.body.transaction_id;
        var displayId = req.body.display_id;
        var title = req.body.title;
        var type = req.body.type;
        var cardType = req.body.card_type;
        var refNo = req.body.ref_no;
        var currency = req.body.currency;


        var pay = new paymentHistory({
            hotel_id: ObjectId(hotelId),
            owner_id: ObjectId(ownerId),
            buyer_id: ObjectId(buyerId),
            booking_id: ObjectId(bookingId),
            amount: amount,
            payment_type: paymentType,
            transaction_id: transactionId,
            display_id: displayId,
            title: title,
            type: type,
            created_at: Date.now(),
            card_type: cardType,
            ref_no: refNo,
            currency: currency,
        })

        pay.save(function (err, data) {
            if (err) {
                console.log("Error>>>>>>>", err);
            } else {
                if (data) {
                    paymentHistory.findOne({ _id: ObjectId(data._id) }, function (err, finalData) {
                        if (err) {
                            console.log("Error>>>>>>>>>", err);
                        } else {
                            res.json({ error: false, "message": "Payment History Created", "data": finalData });
                        }
                    })
                } else {
                    res.json({ error: true, "message": "Payment History not Created" });
                }
            }
        })
    })

    app.post('/getPaymentList', function (req, res) {
        var buyerId = req.body.buyer_id;

        paymentHistory.find({ buyer_id: ObjectId(buyerId) }, function (err, data) {
            if (err) {
                console.log("Error>>>>>>>>>>", err);
            } else {
                if (data) {
                    res.json({ error: false, "message": "Payment List", "data": data });
                } else {
                    res.json({ error: true, "message": "Payment List not Found" });
                }
            }
        })
    })

    app.post('/getSinglePaymentHistory', function (req, res) {
        var paymentId = req.body.payment_id;

        paymentHistory.aggregate([{ $match: { _id: ObjectId(paymentId) } }, { $lookup: { from: 'users', localField: 'owner_id', foreignField: '_id', as: 'ownerData' } },
        { $lookup: { from: 'hotels', localField: 'hotel_id', foreignField: '_id', as: 'hotelData' } },
        { $lookup: { from: 'bookings', localField: 'booking_id', foreignField: '_id', as: 'bookingData' } }],
            function (err, data) {
                if (err) {
                    console.log("Error>>>>>>>>>", err);
                } else {
                    if (data) {
                        res.json({ error: false, "message": "Payment Data", "data": data });
                    } else {
                        res.json({ error: true, "message": "Payment Data not Found" });
                    }
                }
            })
    })

    app.post('/changePassword', function (req, res) {
        var userId = req.body.user_id;
        var newPass = req.body.new_password;

        user.updateOne({ _id: ObjectId(userId) }, { $set: { password: newPass } }, function (err, data) {
            if (err) {
                console.log("Error>>>>>>>>>", err);
            } else {
                if (data) {
                    res.json({ error: false, "message": "Password updated successfully" });
                } else {
                    res.json({ error: true, "message": "Something went wrong" });
                }
            }
        })
    })

    app.post('/facebook', function (req, res) {
        var facebookId = req.body.facebook_id;
        var email = req.body.email;

        user.findOne({ facebook: facebookId }, function (err, data) {
            if (err) {
                console.log("Error>>>>>>>>>>>", err);
            } else {
                console.log("else");
                if (data) {
                    console.log("else if");
                    user.updateOne({ facebook: facebookId }, { $set: { is_login: true } }, function (err, data1) {
                        if (err) {
                            console.log("error>>", err);
                        } else {
                            if (data1) {
                                user.findOne({ facebook: facebookId }, function (err, userDataa) {
                                    if (err) {
                                        console.log("Error >>>>>", err);
                                    } else {
                                        res.json({ "error": false, "message": "user data", "data": userDataa });
                                    }
                                })
                            }
                        }
                    })
                } else {
                    console.log("else else");
                    if (email) {
                        user.findOne({ email: email }, function (err, finalData) {
                            if (err) {
                                console.log("Error>>>>>>>>>>>", err);
                            } else {
                                console.log("else final data");
                                if (finalData) {
                                    console.log("if final data");
                                    user.findOneAndUpdate({ email: email }, { $set: { facebook: facebookId, is_login: true } }, function (err, data2) {

                                        if (err) {
                                            console.log("Error>>>>>>>>>>", err);
                                        } else {
                                            if (data2) {
                                                res.json({ error: false, "message": "User Data", "data": data2 });
                                            } else {
                                                res.json({ error: true, "message": "user not found" });
                                            }
                                        }
                                    })
                                } else {
                                    res.json({ error: true, "message": "user not found" });
                                }
                            }
                        })
                    } else {
                        console.log("err part");
                        res.json({ error: true, "message": "user not found" });
                    }
                }
            }
        })
    })

    // app.post('/getCountries', function(req, res){
    //     //var country = req.body.country;

    //     var data = country.all();
    //     //console.log("Country details is here>>>>>>>>>>>>>",data);
    //     res.json({error: false, "message":"Country Details", "data": data});
    // })

    app.post('/getCountries', function (req, res) {
        let country = [];
        let uniCountry = [];
        let uniCity = [];
        let data = [];
        hotel.find({}, function (err, hotels) {
            if (err) {
                console.log("error>>>>>>>", err);
            } else {
                if (hotels) {
                    //console.log("Hotel Data>>>>>>>>>>>>>>>",hotels);
                    for (let i = 0; i < hotels.length; i++) {
                        country[i] = hotels[i].country;
                        //console.log("non for unique array >>>>>>>>",country);
                    }
                    console.log("non unique array >>>>>>>>", country);
                    uniCountry = Array.from(new Set(country));
                    console.log(" unique array >>>>>>>>", uniCountry);
                    if (uniCountry.length == 0) {
                        res.json({ error: true, "message": "Countries not found" });
                    }

                    for (let j = 0; j < uniCountry.length; j++) {
                        hotel.find({ "country": uniCountry[j] }, { "_id": 0, "city": 1 }, function (err, cityData) {
                            if (err) {
                                console.log("Error>>>>>>>>>>", err);
                            } else {
                                if (cityData) {
                                    console.log("City is here>>>>>>>>>", cityData);
                                    uniCity = Array.from(new Set(cityData.map(x => x.city)));
                                    console.log("uniCity is here>>>>>>>>>", uniCity);
                                    let data1 = { "country": uniCountry[j], "city": uniCity };
                                    data.push(data1);
                                    console.log("uniCity is here>>>>>>>>>", data);
                                    if (data.length == uniCountry.length) {
                                        res.json({ error: false, "message": "Countries", "data": data });
                                    }
                                }
                            }
                        })
                    }
                }
            }
        })
    })

    app.post('/checkEmailAndPhone', function (req, res) {
        var email = req.body.email;
        var phone = req.body.phone;

        if (email) {
            user.findOne({ email: email }, function (err, data) {
                if (err) {
                    console.log("Error>>>>>>>>..", err);
                } else {
                    if (data) {
                        res.json({ error: true, "message": "Email already exists" });
                    } else {
                        user.findOne({ phone: phone }, function (err, finalData) {
                            if (err) {
                                console.log("Error>>>>>>>>.", err);
                            } else {
                                if (finalData) {
                                    res.json({ error: true, "message": "Phone number exists" });
                                } else {
                                    res.json({ error: false, "message": "Everything is fine" });
                                }
                            }
                        })
                    }
                }
            })
        } else {
            if (phone) {
                user.findOne({ phone: phone }, function (err, finalData) {
                    if (err) {
                        console.log("Error>>>>>>>>.", err);
                    } else {
                        if (finalData) {
                            res.json({ error: true, "message": "Phone number exists" });
                        } else {
                            res.json({ error: false, "message": "Everything is fine" });
                        }
                    }
                })
            }
        }

    })

    app.post('/checkEmail', function (req, res) {
        var email = req.body.email;

        if (email) {
            user.findOne({ email: email }, function (err, data) {
                if (err) {
                    console.log("Error>>>>>>>>..", err);
                } else {
                    if (data) {
                        res.json({ error: false, "message": "Email exists" });
                    } else {
                        res.json({ error: true, "message": "Email not exists" });
                    }
                }
            })
        } else {
            res.json({ error: true, "message": "Please provide the email address" });
        }
    })

    app.post('/sendEmailForChangePassword', function (req, res) {
        let emaill = req.body.email;
        // let userId = req.body.user_id;
        mport = 'http://18.191.10.69:4200/#/reset_password'
        //mport = 'http://192.168.0.129:4200/#/reset_password'
        if (emaill) {

            var transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'chandanray876@gmail.com',
                    pass: 'neelam57'
                }
            });

            var str = ` <head>
            <body bgcolor="#e1e5e8" style="margin-top:0 ;margin-bottom:0 ;margin-right:0 ;margin-left:0 ;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px;background-color:#e1e5e8;">
             
              <center style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#e1e5e8;">
                <div style="max-width:600px;margin-top:100px;margin-bottom:0;margin-right:auto;margin-left:auto;">
                  <table align="center" cellpadding="0" style="border-spacing:0;font-family:'Muli',Arial,sans-serif;color:#333333;Margin:0 auto;width:100%;max-width:600px;">
                    <tbody>
                      <tr>
                        <td class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-color:#ffffff;">
                          
                          <table style="border-spacing:0;" width="100%">
                            <tbody>
                              <tr>
                                <td align="center" class="inner" style="padding-top:15px;padding-bottom:15px;padding-right:30px;padding-left:30px;" valign="middle"><span class="sg-image" ><img alt="Forgot Password" class="banner" height="100" src="https://www.freeiconspng.com/uploads/forgot-password-icon-6.jpg" style="border-width: 0px; margin-top: 30px; width: 255px; height: 93px;" width="255"></span></td>
                              </tr>
                              <tr>
                                <td class="inner contents center" style="padding-top:15px;padding-bottom:15px;padding-right:30px;padding-left:30px;text-align:left;">
                                  <center>
                                    <p class="h1 center" style="Margin:0;text-align:center;font-family:'flama-condensed','Arial Narrow',Arial;font-weight:100;font-size:30px;Margin-bottom:26px;">Forgot your password?</p>
                                    
            
                                    <p class="description center" style="font-family:'Muli','Arial Narrow',Arial;Margin:0;text-align:center;max-width:320px;color:#a1a8ad;line-height:24px;font-size:15px;Margin-bottom:10px;margin-left: auto; margin-right: auto;"><span style="color: rgb(161, 168, 173); font-family: Muli,Arial Narrow, Arial; font-size: 15px; text-align: center; background-color: rgb(255, 255, 255);">Click on the button below to reset your password.</span></p>
                                    <span class="sg-image" ><a href= "` + mport + `/` +
                emaill
                +
                `" target="_blank"><img alt="Reset your Password" height="54" src="https://marketing-image-production.s3.amazonaws.com/uploads/c1e9ad698cfb27be42ce2421c7d56cb405ef63eaa78c1db77cd79e02742dd1f35a277fc3e0dcad676976e72f02942b7c1709d933a77eacb048c92be49b0ec6f3.png" style="border-width: 0px; margin-top: 30px; margin-bottom: 50px; width: 260px; height: 54px;" width="260"></a></span>
                                        </center>
                                    </td>
                              </tr>
                            </tbody>
                          </table>
                         
                        </td>
                      </tr>
                     
                      <tr>
                        <td height="25">
                          <p style="line-height: 25px; padding: 0 0 0 0; margin: 0 0 0 0;">&nbsp;</p>
            
                          <p>&nbsp;</p>
                        </td>
                      </tr>
                      <!-- Footer -->
                  </tbody>
                </table>
               
                        </td>
                        </tr>
                    
                        <tr>
                        <td height="25">
                            <p style="line-height: 25px; padding: 0 0 0 0; margin: 0 0 0 0;">&nbsp;</p>
            
                            <p>&nbsp;</p>
                        </td>
                        </tr>
                        <!-- Footer -->
                        <tr>
                        <td height="40">
                            <p style="line-height: 40px; padding: 0 0 0 0; margin: 0 0 0 0;">&nbsp;</p>
            
                            <p>&nbsp;</p>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                </div>
                </center>
            
            
            </body>`;

            var mailOptions = {
                from: 'Kashtahport',
                to: emaill,
                subject: 'Reset Password',
                html: str
            };


            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                } else {
                    console.log('Doctor Email sent: ' + info.response);

                    var strr = info.response;
                    var flag = strr.includes("OK");
                    console.log("outside of else", flag);
                    if (flag == true) {
                        console.log("OK");
                        res.json({ "error": false, "message": "New OTP created successfully" });
                    } else {
                        console.log("NOT OK");
                        res.json({ "error": true, "message": "2Factor Error!" });
                    }
                    // console.log("outside of else");
                }
            });

        }
    })

    app.post('/hotelCheck', function (req, res) {
        var hotelId = req.body.hotel_id;

        hotel.findOne({ _id: ObjectId(hotelId) }, function (err, data) {
            if (err) {
                console.log("Error>>>>>>>>>>>>>>>>>>", err);
            } else {
                if (data && data.is_deleted == false && data.is_approved == true && data.is_enable == true) {
                    res.json({ error: false, "message": "Ok" });
                } else {
                    res.json({ error: true, "message": "Something wrong", "data": data });
                }
            }
        })
    })

    app.post('/updateIBAN', function (req, res) {
        var userId = req.body.user_id;
        var countryCode = req.body.country_code;
        var iban = req.body.iban;
        var bankName = req.body.bank_name
        var phone = req.body.phone;

        if (phone && bankName && iban && countryCode) {
            var IBAN = {
                country_code: countryCode,
                IBAN: iban,
                bank_name: bankName,
                phone: phone
            }

            user.updateOne({ _id: ObjectId(userId) }, { $set: { IBAN: IBAN } }, function (err, updated) {
                if (err) {
                    console.log("Error>>>>>>>>>", err);
                } else {
                    if (updated) {
                        user.findOne({ _id: ObjectId(userId) }, function (err, data) {
                            if (err) {
                                console.log("Error>>>>>>>>>>", err);
                            } else {
                                if (data) {
                                    res.json({ error: false, "message": "IBAN Updated", "data": data });
                                } else {
                                    res.json({ error: true, "message": "Something went wrong" });
                                }
                            }
                        })
                    } else {
                        res.json({ error: true, "message": "Something went wrong" });
                    }
                }
            })
        }
    })

    app.post('/updateNotificationToken', function (req, res) {
        var userId = req.body.user_id;
        var notificationToken = req.body.notification_token;
        var deviceType = req.body.device_type;

        user.updateOne({ _id: ObjectId(userId) }, { $set: { notification_token: notificationToken, device_type: deviceType } }, function (err, data) {
            if (err) {
                console.log("Error>>>>>>>>>>", err);
            } else {
                if (data) {
                    res.json({ error: false, "message": "notification token updated" });
                } else {
                    res.json({ error: true, "message": "Something went wrong" });
                }
            }
        })
    })

    app.post('/updateNotificationEnable', function (req, res) {
        var userId = req.body.user_id;
        var enable = req.body.enable;

        user.updateOne({ _id: ObjectId(userId) }, { $set: { notification_enable: enable } }, function (err, data) {
            if (err) {
                console.log("Error>>>>>>>>>>", err);
            } else {
                if (data) {
                    res.json({ error: false, "message": "notification enable " });
                } else {
                    res.json({ error: true, "message": "Something went wrong" });
                }
            }
        })
    })

    app.post('/time', function (req, res) {
        let t = Date.now();
        console.log("Date is >>>>>>>>>>", t, ">>>>>>>>>>>", typeof t);
    })

    app.post('/testNotification', function (req, res) {
        var noti_token = req.body.notification_token;

        // notification.sendNotificationForIOS([noti_token], "hello from outside","Your booking was comming close","message","Booking");
        notification.sendNotification([noti_token], "hello from outside!!", function (data) {
            console.log("Data>>>>>>>>", data);
        });

        notification.sendNotificationForIOS([noti_token], "asdfghjkl", "Your booking is comming close", "Booking", "Booking", function (data) {

        })

    })

    app.post('/deleteSingleActivity', function (req, res) {
        var activityId = req.body.activity_id;
        var userId = req.body.user_id;


        activity.remove({ _id: ObjectId(activityId), user_id: ObjectId(userId) }, function (err, data) {
            if (err) {
                console.log("Error>>>>>>>>>>>>>>>>>>>", err);
            } else {
                if (data) {
                    res.json({ error: false, "message": "Activity deleted successfully" });
                } else {
                    res.json({ error: true, "message": "Something went wrong" });
                }
            }
        })
    })

    app.post('/deleteAllActivity', function (req, res) {
        var userId = req.body.user_id;

        activity.remove({ user_id: ObjectId(userId) }, function (err, data) {
            if (err) {
                console.log("Error>>>>>>>>>>>>>>>>>>>", err);
            } else {
                if (data) {
                    res.json({ error: false, "message": "Activity deleted successfully" });
                } else {
                    res.json({ error: true, "message": "Something went wrong" });
                }
            }
        })
    })

    app.post('/updateCount', function (req, res) {
        var userId = req.body.user_id;
        var count = req.body.count;

        user.updateOne({ _id: ObjectId(userId) }, { $set: { count: count } }, function (err, data) {
            if (err) {
                console.log("Error>>>>>>>>>>>>>>>>>>>", err);
            } else {
                if (data) {
                    res.json({ error: false, "message": "count updated successfully", "data": data });
                } else {
                    res.json({ error: true, "message": "Something went wrong" });
                }
            }
        })
    })

    app.post('/detect', function (req, res) {
        var str = req.body.str;

        console.log(lngDetector.detect(str, 1));

        googleTranslate.translate(['جايبور', 'باتنا', 'الهند'], 'en', function (err, translation) {
            if (err) {
                console.log("Error>>>>>>>>>>", err);
            } else {


                console.log("Translated in english>>>>>>>>>>", translation);
                // =>  Mi nombre es Brandon
            }
        });

    })


    app.post('/voice.xml', function (req, res) {
        // res.render('aboutus');
        res.sendFile(path.join(__dirname + '/voice.xml'));
        // http://localhost:8080/aboutUs  sharable link
    })

    app.get('/voice.xml', function (req, res) {
        // res.render('aboutus');
        res.sendFile(path.join(__dirname + '/voice.xml'));
        // http://localhost:8080/aboutUs  sharable link
    })

    app.post('/makeCall', function (req, res) {

        console.log("IN the makeCall API");
        var phone = req.body.phone;
        var country_code = req.body.country_code;

        var fs = require("fs");

        var otp1 = Math.floor(100000 + Math.random() * 900000);
        var stringOtp = otp1.toString();
        var splitOtp = stringOtp.split('');

        let registerOTP = new otp({
            phone: phone,
            otp: otp1,
            created_at: Date.now().toString(),
            expire_at: (Date.now() + 7200000).toString()
        })

        var data = `<Response><Say voice='man' language='en' loop='2'>Your OTP for kashtahport is:  ${splitOtp} </Say></Response>`;

        
        fs.writeFile(__dirname+"/voice.xml", data, (err) => {
            if (err) console.log(err);
            console.log("Successfully Written to File.");
        });

        user.findOne({ phone: phone }, function (err, doc) {
            if (err) {
                res.json({ "error": true, "message": "Database Error!" });
            } else {
                if (doc) {
                    // console.log("In Doc");
                    if (doc && doc.is_blocked) {
                        // console.log("In Doc and doc.is_blocked");
                        res.json({ "error": true, "message": "Account BLOCKED!!! Please contact admin." });
                    } else {
                        //Delete past OTPs
                        //console.log("In else of Doc and doc.is_blocked");
                        otp.deleteMany({ phone: phone }, function (err, deleteDone) {
                            if (err) {
                                res.json({ "error": true, "message": "Database Error!" });
                            } else {
                                if (deleteDone) {
                                    console.log("Deleting old otp is successful", deleteDone);
                                }

                                registerOTP.save(function (err, otpData) {
                                    if (err) {
                                        //res.json({ error: true, message: "Something went wrong, Please try again", data: err })
                                    } else {
                                        str = "Your OTP for KASHTAHPORT is: " + otp1;
                                        // request 
                                        console.log("new otp is saved", otpData);
                                        commonMethodes.data.call(phone, country_code);
                                        res.json({ error: false, message: "OTP has been sent.", data: otpData });
                                    }
                                })
                            }
                        })
                    }
                } else {
                    console.log("If User Not Find By Phone");
                    otp.deleteMany({ phone: phone }, function (err, deleteDone) {
                        if (err) {
                            res.json({ "error": true, "message": "Database Error!" });
                        } else {
                            if (deleteDone) {
                                console.log("Deleting old otp is successful if user not found", deleteDone);
                            }

                            registerOTP.save(function (err, otpData) {
                                if (err) {
                                    //res.json({ error: true, message: "Something went wrong, Please try again", data: err })
                                } else {
                                    str = "Your OTP for KASHTAHPORT is: " + otp1;
                                    // request 
                                    commonMethodes.data.call(phone, country_code);
                                    res.json({ error: false, message: "OTP has been sent.", data: otpData });
                                }
                            })
                        }
                    })
                }
            }
        })
    })






    // admin api's

    app.post('/loginAdminA', admin.loginAdmin);

    app.post('/getSingleUserA', admin.getSingleUser);

    app.post('/listUserA', admin.listUser);

    app.post('/listRegexUserA', admin.listRegexUser);

    app.post('/CountUsersA', admin.CountUsers);

    app.post('/CountHotelsA', admin.CountHotels);

    app.post('/CountBlockedUsersA', admin.CountBlockedUsers);

    app.post('/CountUnapprovedHotelsA', admin.CountUnapprovedHotels);

    app.post('/listBlockUserA', admin.listBlockUser);

    app.post('/listBlockRegexUserA', admin.listBlockRegexUser);

    app.post('/listFarmA', admin.listFarm);

    app.post('/listCampA', admin.listCamp);

    app.post('/listChaletA', admin.listChalet);

    app.post('/listRegexFarmA', admin.listRegexFarm);

    app.post('/listRegexCampA', admin.listRegexCamp);

    app.post('/listRegexChaletA', admin.listRegexChalet);

    app.post('/listUnapprovedHotelA', admin.listUnapprovedHotel);

    app.post('/listBookingA', admin.listBooking);

    app.post('/listFeedbackA', admin.listFeedback);

    app.post('/approvedHotelA', admin.approvedHotel);

    app.post('/deleteUnapprovedHotelA', admin.deleteUnapprovedHotel);

    app.post('/listTransactionA', admin.listTransaction);

    app.post('/listBookingByUserA', admin.listBookingByUser);

    app.post('/listPropertyByUserA', admin.listPropertyByUser);

    app.post('/updateBlockA', admin.updateBlock);

    app.post('/getSingleHotelA', admin.getSingleHotel);

    app.post('/getBookingByHotelA', admin.getBookingByHotel);

    app.post('/getTransactionByHotelA', admin.getTransactionByHotel);

    app.post('/setTransferStatusA', admin.setTransferStatus);

    app.post('/changePasswordA', admin.changePassword);

    app.post('/getBookingByIdA', admin.getBookingById);

    app.post('/changePasswordByEmailA', admin.changePasswordByEmail);

    app.post('/sumTransactionA', admin.sumTransaction);

    app.post('/getUserInSevenDayA', admin.getUserInSevenDay);

    app.post('/getTransactionInDaysA', admin.getTransactionInDays);



}

