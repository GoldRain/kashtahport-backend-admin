var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var moment = require('moment');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/hotel',{ useNewUrlParser: true });

var nowDate = new Date(); 
nowDate.setHours(0, 0, 0, 0);
var UserSchema = new Schema({
  name : {type: String, required: true, default: ""},
  email : {type: String, default: ""},
  facebook : {type: String, default: ""},
  password : {type: String, default: ""},
  phone : {type: String, default: ""},   //new added
  type : {type: String, default: ""},   //new added
  profile_image_url : {type: String, default: ""},
  device_id : {type: String, default: ""},
  device_type : {type: String, default: ""},
  // for push notification on one signal
  notification_token : {type: String, default: ""},
  access_token : {type: String, default: ""},   //new added
  socket_id : {type: Object, default: ""}, //always update on every login time
  online_status : {type: Boolean, default: true}, //if socket id is available then it set to online else offline
  is_login : {type: Boolean, default: true}, //for login and logout
  is_blocked : {type: Boolean, default: false},   //new added
  created_at : {type: Number, default: Date.now()},
  date_for_fetch : {type: String, default: moment().format("YYYY-MM-DD")},
  last_login : {type: String, default: ""},
  favourites: [{type: Object, default: ""}],
  can_sale: {type: Boolean, default: false},
  country_code: {type: String, default: ""},
  description: {type: String, default: ""},
  profession: {type: String, default: ""},
  IBAN :{},
  notification_enable : {type: Boolean, default: false},
  count : {type: String, default: ''},
});

var User = mongoose.model('user', UserSchema);

module.exports = User;