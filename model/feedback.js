var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var FeedbackSchema = new Schema({
    user_id : {type: Object, default: ""},
    hotel_id : {type : Object, default : ""},
    booking_id : {type: Object, default: ""},
    communication : {  type: Number, default:0 },
    price : {  type: Number, default:0 },
    delivery : {  type: Number, default:0 },
    proficiency : {  type: Number, default:0 },
    created_at : { type: String, default: Date.now()},
    comment: {type: String, default: ""}
  });
  
  var Feedback = mongoose.model('feedback', FeedbackSchema);
  
  module.exports = Feedback;