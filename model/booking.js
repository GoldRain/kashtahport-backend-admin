var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var BookingSchema = new Schema({
    owner_id : {type: Object, default: ""},
    buyer_id : {type: Object, default: ""},
    hotel_id : {type : Object, default : ""},
    childrens : {type : String, default : ""},
    adults: {type : String, default : ""},
    rooms: {type: String, default:""},
    status: {type: String, default: "pending" },
    price: {type: String, default: ""},
    date_from: {type: String, default: ""},
    date_to: {type: String, default: ""},
    created_at: {type: String, default: Date.now()},
    feedback_given : {type: Boolean, default: false},
  });
  
  var Booking = mongoose.model('booking', BookingSchema);
  
  module.exports = Booking;