var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OtpSchema = new Schema({
    phone : {type: String, required: true, default: ""},
    otp : {type: String, required: true, default: ""},
    created_at : {type: String, required: true},
    expire_at : {type: String, required: true},
    access_token : {type: String, default: ""}
  });
  
  var Otp = mongoose.model('otps', OtpSchema);
  
  module.exports = Otp;