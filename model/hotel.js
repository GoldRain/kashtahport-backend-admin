var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var HotelSchema = new Schema({
    owner_id : {type: Object, required: true, default: ""},
    type : {type: String, default: ""},
    name : {type : String, default : ""},
    city : {type : String, default : ""},
    country: {type : String, default : ""},
    pictures: [{type: String, default:""}],
    rooms_number: {type: String, default: ""},
    wc_number: {type: String, default: ""},
    person_number: {type: String, default:""},
    children: {type: String, default:""},
    amenities: [],
    conditions: [{type: String, default: ""}],
    available_date: [],
    rate: {type: String, default: ""},
    is_enable: {type: Boolean, default: false },
    location: {
      type:{
        type: String,
        default: "Point"
      },
      coordinates: [0, 0]
    },
    rating : {  type: Number, default:1 },
    is_approved: {type: Boolean, default: false },
    created_at : {type: String , default: Date.now()},
    updated_at : {type: String, default: ""},
    longitude : {  type: Number, default:0 },
    latitude : {  type: Number, default:0 },
    description : {type: String, default: ""},
    address : {type: String, default: ""},
    contact_info : {type: String, default: ""},
    is_deleted : {type: Boolean, default: false},
    currency :{type: Object, default: ""},
  });
  
  var Hotel = mongoose.model('hotel', HotelSchema);
  
  module.exports = Hotel;