var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var moment = require('moment');

var PaymentHistorySchema = new Schema({
 hotel_id: {type: Object, default: ""},
 owner_id: {type: Object, default: ""},
 buyer_id: {type: Object, default: ""},
 booking_id: {type: Object, default: ""},
 amount: {type: Number, default: ""},
 payment_type: {type: String, default: ""},
 transaction_id: {type: String, default: ""},
 display_id: {type: String, default: ""},
 created_at: {type: String, default: ""},
 title: {type: String, default: ""},
 date_for_fetch : {type: String, default: moment().format("YYYY-MM-DD")},
 type: {type: String, default: ""},
 ref_no: {type: String, default: ""},
 card_type: {type: String, default: ""},
 status: {type:Boolean, default: false},
 card_detail: [],
 currency: {type: String, default: ""},
});

var PaymentHistory = mongoose.model('paymentHistory', PaymentHistorySchema);

module.exports = PaymentHistory;