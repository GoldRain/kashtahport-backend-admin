var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var moment = require('moment');

var ScheduleBookingSchema = new Schema({
    owner_id: { type: Object, default: "" },
    buyer_id: { type: Object, default: "" },
    hotel_id: { type: Object, default: "" },
    booking_id: { type: Object, default: "" },
    created_at: { type: String, default: "" },
    scheduled_at: {type : String, required : true},
});

var ScheduleBooking = mongoose.model('scheduleBooking', ScheduleBookingSchema);

module.exports = ScheduleBooking;