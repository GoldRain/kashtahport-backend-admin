var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ActivitySchema = new Schema({
  user_id : {type: Object, default: ""},
  hint_id : {type: Object, default: ""},
  hotel_id : {type: Object, default: ""},   
  hint : {type : String, default: ""},
  message : {type: String, default: ""},
  created_at : {type:String,default:""},
});

var Activity = mongoose.model('activity', ActivitySchema);

module.exports = Activity;